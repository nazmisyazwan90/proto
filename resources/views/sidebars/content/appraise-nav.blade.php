<li class="nav-item has-treeview {!! classActivePath(1, 'appr') !!}">
    <a href="#" class="nav-link {!! classActiveSegment(1, 'appr') !!}">
      <i class="nav-icon fa fa-book"></i>
      <p>
        Appraiser
        <i class="fa fa-angle-left right"></i>
      </p>
    </a>
    <ul class="nav nav-treeview">
        <li class="nav-item">
            <a href={{ url('/appr/appr-dash') }} class="nav-link {!! classActiveSegment(2, 'appr-dash') !!} treeview-first">
                <i class="fa fa-circle-o nav-icon"></i>
                <p>Dashboard</p>
            </a>
        </li>
        <li class="nav-item">
            <a href={{ url('/appr/appraise-list') }} class="nav-link {!! classActiveSegment(2, 'appraise-list') !!} treeview-first">
                <i class="fa fa-circle-o nav-icon"></i>
                <p>Staff Appraisal List</p>
            </a>
        </li>
        <li class="nav-item">
            <a href={{ url('/appr/staff-appraisal') }} class="nav-link {!! classActiveSegment(2, 'staff-appraisal') !!} treeview-first">
                <i class="fa fa-circle-o nav-icon"></i>
                <p>Appraisal Form</p>
            </a>
        </li>
    </ul>
</li>