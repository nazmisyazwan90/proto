<li class="nav-item has-treeview {!! classActivePath(1, 'extras') !!}">
    <a href="#" class="nav-link {!! classActiveSegment(1, 'extras') !!}">
      <i class="nav-icon fas fa-plus-square"></i>
      <p>
        Extras
        <i class="fa fa-angle-left right"></i>
      </p>
    </a>
    <ul class="nav nav-treeview">
        <li class="nav-item">
            <a href={{ url('/extras/404') }} class="nav-link {!! classActiveSegment(2, '404') !!} treeview-first">
                <i class="fa fa-circle-o nav-icon"></i>
                <p>Error 404</p>
            </a>
        </li>
        <li class="nav-item">
            <a href={{ url('/extras/500') }} class="nav-link {!! classActiveSegment(2, '500') !!} treeview-first">
                <i class="fa fa-circle-o nav-icon"></i>
                <p>Error 500</p>
            </a>
        </li>
        <li class="nav-item">
            <a href={{ url('/extras/blank') }} class="nav-link {!! classActiveSegment(2, 'blank') !!} treeview-first">
                <i class="fa fa-circle-o nav-icon"></i>
                <p>Blank Page</p>
            </a>
        </li>
        <li class="nav-item">
            <a href={{ url('/extras/starter') }} class="nav-link treeview-first">
                <i class="fa fa-circle-o nav-icon"></i>
                <p>Starter Page</p>
            </a>
        </li>
    </ul>
</li>