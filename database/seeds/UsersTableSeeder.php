<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Nazmi Syazwan',
            'password' => bcrypt('123456'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'username' => 'nazmi',
            'joined_date' => Carbon::create('2018', '05', '22'),
            'staff_type' => 'Technical',
            'pos_id' => 'P001',
            'dept_id' => 'D002',
            'last_promotion_date' => Carbon::create('2016', '06', '15'),
            'staff_role' => ''
        ]);
    }
}
