<?php

use Illuminate\Database\Seeder;

class DepartmentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('departments')->insert([
            'dept_id' => 'D001',
            'dept_name' => 'R&D',                    
        ]);

        DB::table('departments')->insert([
            'dept_id' => 'D002',
            'dept_name' => 'Internal',                    
        ]);

    }
}
