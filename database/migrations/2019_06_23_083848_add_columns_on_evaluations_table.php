<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsOnEvaluationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('evaluations', function (Blueprint $table){
            $table->dropColumn('eval_remarks');

            $table->boolean('self_appr_status')->default(0);
            $table->longtext('remarks_str');
            $table->longtext('remarks_weak');
            $table->longtext('remarks_goals');
            $table->longtext('remarks_other');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('evaluations', function (Blueprint $table){
            $table->dropColumn('self_appr_status');
            $table->dropColumn('remarks_str');
            $table->dropColumn('remarks_weak');
            $table->dropColumn('remarks_goals');
            $table->dropColumn('remarks_other');

            $table->longText('eval_remarks')->nullable();
        });
    }
}
