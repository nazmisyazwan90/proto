<li class="nav-item has-treeview {!! classActivePath(1, 'mailbox') !!}">
    <a href="#" class="nav-link {!! classActiveSegment(1, 'mailbox') !!}">
      <i class="nav-icon fas fa-envelope-open"></i>
      <p>
        Mailbox
        <i class="fa fa-angle-left right"></i>
      </p>
    </a>
    <ul class="nav nav-treeview">
        <li class="nav-item">
            <a href={{ url('/mailbox/inbox') }} class="nav-link {!! classActiveSegment(2, 'inbox') !!} treeview-first">
                <i class="fa fa-circle-o nav-icon"></i>
                <p>Inbox</p>
            </a>
        </li>
        <li class="nav-item">
            <a href={{ url('/mailbox/compose') }} class="nav-link {!! classActiveSegment(2, 'compose') !!} treeview-first">
                <i class="fa fa-circle-o nav-icon"></i>
                <p>Compose</p>
            </a>
        </li>
        <li class="nav-item">
            <a href={{ url('/mailbox/readmail') }} class="nav-link {!! classActiveSegment(2, 'readmail') !!} treeview-first">
                <i class="fa fa-circle-o nav-icon"></i>
                <p>Read</p>
            </a>
        </li>
    </ul>
</li>