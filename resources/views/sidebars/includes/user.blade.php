<!-- Sidebar user panel (optional) -->
@guest
@else
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
            <img src="/img/profile.png" class="img-circle elevation-2" alt="User Image">
        </div>
        @if (Auth::user()->name != NULL)
            <div class="info">
                <a href={{ url('/pages/profile') }} class="d-block"> 
                    @php
                        $username = Auth::user()->name;
                        if ($username != null){
                            $firstname = explode(' ', $username);
                        }   
                    @endphp
                    {{ $firstname[0] != null ? $firstname[0] : "Administrator"}} 
                </a>
            </div>
        @elseif (Auth::user()->username != NULL)
        <div class="info">
            <a href={{ url('/pages/profile') }} class="d-block">{{ Auth::user()->username != NULL ? auth()->user()->username : '' }}
        </div>
        @endif
    </div>
@endguest