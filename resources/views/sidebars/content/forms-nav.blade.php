<li class="nav-item has-treeview {!! classActivePath(1, 'forms') !!}">
  <a href="#" class="nav-link {!! classActiveSegment(1, 'forms') !!}">
    <i class="nav-icon fa fa-edit"></i>
    <p>
      Forms
      <i class="fa fa-angle-left right"></i>
    </p>
  </a>
  <ul class="nav nav-treeview">
    <li class="nav-item">
      <a href={{ url('/forms/generalelements') }} class="nav-link {!! classActiveSegment(2, 'generalelements') !!} treeview-first">
        <i class="fa fa-circle-o nav-icon"></i>
        <p>General Elements</p>
      </a>
    </li>
    <li class="nav-item">
      <a href={{ url('/forms/advanced') }} class="nav-link {!! classActiveSegment(2, 'advanced') !!} treeview-first">
        <i class="fa fa-circle-o nav-icon"></i>
        <p>Advanced Elements</p>
      </a>
    </li>
    <li class="nav-item">
      <a href={{ url('/forms/editors') }} class="nav-link {!! classActiveSegment(2, 'editors') !!} treeview-first">
        <i class="fa fa-circle-o nav-icon"></i>
        <p>Editors</p>
      </a>
    </li>
  </ul>
</li>