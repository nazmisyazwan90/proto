@extends('layouts.mastercontent')
@section('newContent')
    <div class="wrapper" id="app">
            <!-- Header -->
        @include('layouts.header')
        
        <div class="sidebar nano">
            <div class="nano-content">
                <nav class=""></nav>
            </div>
        </div>

        @include('layouts.footer')
    </div>
    <!-- wrapper -->
@endsection