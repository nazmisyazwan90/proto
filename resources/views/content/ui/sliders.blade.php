@extends('layouts.mastercontent')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
            <h1>Sliders</h1>
            </div>
            <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item active">Sliders</li>
            </ol>
            </div>
        </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
        <div class="row">
            <div class="col-12">
            <div class="card card-primary">
                <div class="card-header">
                <h3 class="card-title">Ion Slider</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                <div class="row margin">
                    <div class="col-sm-6">
                    <input id="range_1" type="text" name="range_1" value="">
                    </div>

                    <div class="col-sm-6">
                    <input id="range_2" type="text" name="range_2" value="1000;100000" data-type="double"
                            data-step="500" data-postfix=" &euro;" data-from="30000" data-to="90000" data-hasgrid="true">
                    </div>
                </div>
                <div class="row margin">
                    <div class="col-sm-6">
                    <input id="range_5" type="text" name="range_5" value="">
                    </div>
                    <div class="col-sm-6">
                    <input id="range_6" type="text" name="range_6" value="">
                    </div>
                </div>
                <div class="row margin">
                    <div class="col-sm-12">
                    <input id="range_4" type="text" name="range_4" value="10000;100000">
                    </div>
                </div>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->

        <div class="row">
            <div class="col-12">
            <div class="card card-primary">
                <div class="card-header">
                <h3 class="card-title">Bootstrap Slider</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                <div class="row margin">
                    <div class="col-sm-6">
                    <input type="text" value="" class="slider form-control" data-slider-min="-200" data-slider-max="200"
                            data-slider-step="5" data-slider-value="[-100,100]" data-slider-orientation="horizontal"
                            data-slider-selection="before" data-slider-tooltip="show" data-slider-id="red">

                    <p>data-slider-id="red"</p>
                    <input type="text" value="" class="slider form-control" data-slider-min="-200" data-slider-max="200"
                            data-slider-step="5" data-slider-value="[-100,100]" data-slider-orientation="horizontal"
                            data-slider-selection="before" data-slider-tooltip="show" data-slider-id="blue">

                    <p>data-slider-id="blue"</p>
                    <input type="text" value="" class="slider form-control" data-slider-min="-200" data-slider-max="200"
                            data-slider-step="5" data-slider-value="[-100,100]" data-slider-orientation="horizontal"
                            data-slider-selection="before" data-slider-tooltip="show" data-slider-id="green">

                    <p>data-slider-id="green"</p>
                    <input type="text" value="" class="slider form-control" data-slider-min="-200" data-slider-max="200"
                            data-slider-step="5" data-slider-value="[-100,100]" data-slider-orientation="horizontal"
                            data-slider-selection="before" data-slider-tooltip="show" data-slider-id="yellow">

                    <p>data-slider-id="yellow"</p>
                    <input type="text" value="" class="slider form-control" data-slider-min="-200" data-slider-max="200"
                            data-slider-step="5" data-slider-value="[-100,100]" data-slider-orientation="horizontal"
                            data-slider-selection="before" data-slider-tooltip="show" data-slider-id="aqua">

                    <p>data-slider-id="aqua"</p>
                    <input type="text" value="" class="slider form-control" data-slider-min="-200" data-slider-max="200"
                            data-slider-step="5" data-slider-value="[-100,100]" data-slider-orientation="horizontal"
                            data-slider-selection="before" data-slider-tooltip="show" data-slider-id="purple">

                    <p style="margin-top: 10px">data-slider-id="purple"</p>
                    </div>
                    <div class="col-sm-6 text-center">
                    <input type="text" value="" class="slider form-control" data-slider-min="-200" data-slider-max="200"
                            data-slider-step="5" data-slider-value="[-100,100]" data-slider-orientation="vertical"
                            data-slider-selection="before" data-slider-tooltip="show" data-slider-id="red">
                    <input type="text" value="" class="slider form-control" data-slider-min="-200" data-slider-max="200"
                            data-slider-step="5" data-slider-value="[-100,100]" data-slider-orientation="vertical"
                            data-slider-selection="before" data-slider-tooltip="show" data-slider-id="blue">
                    <input type="text" value="" class="slider form-control" data-slider-min="-200" data-slider-max="200"
                            data-slider-step="5" data-slider-value="[-100,100]" data-slider-orientation="vertical"
                            data-slider-selection="before" data-slider-tooltip="show" data-slider-id="green">
                    <input type="text" value="" class="slider form-control" data-slider-min="-200" data-slider-max="200"
                            data-slider-step="5" data-slider-value="[-100,100]" data-slider-orientation="vertical"
                            data-slider-selection="before" data-slider-tooltip="show" data-slider-id="yellow">
                    <input type="text" value="" class="slider form-control" data-slider-min="-200" data-slider-max="200"
                            data-slider-step="5" data-slider-value="[-100,100]" data-slider-orientation="vertical"
                            data-slider-selection="before" data-slider-tooltip="show" data-slider-id="aqua">
                    <input type="text" value="" class="slider form-control" data-slider-min="-200" data-slider-max="200"
                            data-slider-step="5" data-slider-value="[-100,100]" data-slider-orientation="vertical"
                            data-slider-selection="before" data-slider-tooltip="show" data-slider-id="purple">
                    </div>
                </div>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection