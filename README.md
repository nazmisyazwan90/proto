# Staff Appraisal System

* make sure xampp apache & mysql is running

# terminal commands:
* add a database through phpmyadmin called "admingit" before running any migration
* run db migrations "php artisan migrate:fresh" --> this line should delete all tables in the database and recreate the tables
* run db seeds "php artisan db:seed" --> initial login credentials
* run "php artisan serve" to run the web app, goto browser and type in the address "http://127.0.0.1:8000"

# testing
* login: nazmi
* password: 123456

# comitting / pushing changes
* check on source control
* any changes made locally need to be committed, left side of vscode, 3rd icon down
* enter update messages in the box, and press ctrl+enter to commit
* then run "git push" on the terminal

