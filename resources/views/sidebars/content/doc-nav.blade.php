<li class="nav-item has-treeview">
    <a href="" class="nav-link">
      <i class="nav-icon fa fa-file"></i>
      <p>
        Documentation
        <i class="fa fa-angle-left right"></i>
      </p>
    </a>
    <ul class="nav nav-treeview">
        <li class="nav-item">
            <a href="https://adminlte.io/docs" class="nav-link treeview-first" target="_blank">
              <i class="nav-icon fa fa-file"></i>
              <p>AdminLTE Doc</p>
            </a>
        </li>
          <li class="nav-item">
            <a href="https://laracasts.com/skills/laravel" class="nav-link treeview-first" target="_blank">
              <i class="nav-icon fa fa-file"></i>
              <p>Laracasts</p>
            </a>
        </li>
        <li class="nav-item">
            <a href="https://laravel.com/docs/5.8" class="nav-link treeview-first" target="_blank">
                <i class="nav-icon fa fa-file"></i>
                <p>Laravel Doc</p>
            </a>
        </li>
    </ul>
</li>