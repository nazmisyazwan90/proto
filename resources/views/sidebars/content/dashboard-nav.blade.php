<li class="nav-item has-treeview {!! classActivePath(1,'dashboard') !!}">
    <a href="#" class="nav-link {!! classActiveSegment(1, 'dashboard') !!}">
      <i class="nav-icon fas fa-tachometer-alt"></i>
      <p>
        Dashboard
        <i class="right fa fa-angle-left"></i>
      </p>
    </a>
    <ul class="nav nav-treeview">
        <li class="nav-item">
            <a href="{{ route('v1') }}" class="nav-link {!! classActiveSegment(2, 'home') !!} treeview-first">
              <i class="fa fa-circle-o nav-icon"></i>
              <p>Dashboard v1</p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{ route('v2') }}" class="nav-link {!! classActiveSegment(2, 'v2') !!} treeview-first">
              <i class="fa fa-circle-o nav-icon"></i>
              <p>Dashboard v2</p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{ route('v3') }}" class="nav-link {!! classActiveSegment(2, 'v3') !!} treeview-first">
              <i class="fa fa-circle-o nav-icon"></i>
              <p>Dashboard v3</p>
            </a>
        </li>
    </ul>
</li>