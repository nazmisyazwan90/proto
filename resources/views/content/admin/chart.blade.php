@extends('layouts.mastercontent')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
            <h1>Chart</h1>
            </div>
            <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item active">Blank Page</li>
            </ol>
            </div>
        </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        
        <div class="row col-12">
            <div class="col-6">
                <h1 class="mt-2 ml-2 mb-4">Band Score</h1>
            </div>
            
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row"><!-- /.guidelines row -->
                            <div class="col-12">
                                <div class="card">
                              
                                <!-- /.card-header -->
                                <div class="card-body">
                                    <div class="tab-content">
                                        <div class="tab-pane active table-responsive p-0" id="capability">
                                            <table class="table table-hover table-striped table-bordered">
                                            <h1>Band Score</h1>
                                                <thead>
                                                    <th>Band</th>
                                                    <th>Count</th>
                                                    <th>DEP</th>
                                                    <th>LN</th>
                                                    <th>BI</th>
                                                    <th>EB</th>
                                                    <th>ICBA 10</th>
                                                    <th>TRS</th>
                                                    <th>QA</th>
                                                    <th>OPQM</th>
                                                    <th>S&M</th>
                                                    
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <th >A</th>
                                                        <td >0</td>
                                                        <td >0</td>
                                                        <td >0</td>
                                                        <td >0</td>
                                                        <td >0</td>
                                                        <td >0</td>
                                                        <td >0</td>
                                                        <td >0</td>
                                                        <td >0</td>
                                                        <td >0</td>
                                                    </tr>
                                                    <tr>
                                                    <th >B</th>
                                                        <td >0</td>
                                                        <td >0</td>
                                                        <td >0</td>
                                                        <td >0</td>
                                                        <td >0</td>
                                                        <td >0</td>
                                                        <td >0</td>
                                                        <td >0</td>
                                                        <td >0</td>
                                                        <td >0</td>
                                                    <tr>
                                                    <th >C</th>
                                                        <td >0</td>
                                                        <td >0</td>
                                                        <td >0</td>
                                                        <td >0</td>
                                                        <td >0</td>
                                                        <td >0</td>
                                                        <td >0</td>
                                                        <td >0</td>
                                                        <td >0</td>
                                                        <td >0</td>                                      
                                                    </tr>
                                                    <tr>
                                                    <th >D</th>
                                                        <td >0</td>
                                                        <td >0</td>
                                                        <td >0</td>
                                                        <td >0</td>
                                                        <td >0</td>
                                                        <td >0</td>
                                                        <td >0</td>
                                                        <td >0</td>
                                                        <td >0</td>
                                                        <td >0</td>
                                                    </tr>
                                                    <tr>
                                                    <th >E</th>
                                                        <td >0</td>
                                                        <td >0</td>
                                                        <td >0</td>
                                                        <td >0</td>
                                                        <td >0</td>
                                                        <td >0</td>
                                                        <td >0</td>
                                                        <td >0</td>
                                                        <td >0</td>
                                                        <td >0</td>
                                                    </tr>
                                                    <tr>
                                                    <th>Total<th>
                                                        <td>0</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        
                                        
                                    </div>
                                </div>
                                <!-- /.card-body -->
                                 <!-- /.card-header -->
                                 <div class="card-body">
                                    <div class="tab-content">
                                        <div class="tab-pane active table-responsive p-0" id="capability">
                                            <table class="table table-hover table-striped table-bordered">
                                            <h1>Banding Distribution Across Department</h1>
                                                <thead>
                                                    <th>Band</th>
                                                    <th>LN</th>
                                                    <th>BI</th>
                                                    <th>EB</th>
                                                    <th>ICBA 10</th>
                                                    <th>TRS</th>
                                                    <th>QA</th>
                                                    <th>OPQM</th>
                                                    <th>S&M</th>
                                                    
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <th >A</th>
                                                        <td >0</td>
                                                        <td >0</td>
                                                        <td >0</td>
                                                        <td >0</td>
                                                        <td >0</td>
                                                        <td >0</td>
                                                        <td >0</td>
                                                        <td >0</td>
                                                    </tr>
                                                    <tr>
                                                    <th >B</th>
                                                        <td >0</td>
                                                        <td >0</td>
                                                        <td >0</td>
                                                        <td >0</td>
                                                        <td >0</td>
                                                        <td >0</td>
                                                        <td >0</td>
                                                        <td >0</td>
                                                    <tr>
                                                    <th >C</th>
                                                        <td >0</td>
                                                        <td >0</td>
                                                        <td >0</td>
                                                        <td >0</td>
                                                        <td >0</td>
                                                        <td >0</td>
                                                        <td >0</td>
                                                        <td >0</td>                                      
                                                    </tr>
                                                    <tr>
                                                    <th >D</th>
                                                        <td >0</td>
                                                        <td >0</td>
                                                        <td >0</td>
                                                        <td >0</td>
                                                        <td >0</td>
                                                        <td >0</td>
                                                        <td >0</td>
                                                        <td >0</td>
                                                    </tr>
                                                    <tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        
                                        
                                    </div>
                                </div>

                                <!-- /.card-header -->
                                <div class="card-body">
                                    <div class="tab-content">
                                        <div class="tab-pane active table-responsive p-0" id="capability">
                                            <table class="table table-hover table-striped table-bordered">
                                            <h1>Banding Distribution Within Department</h1>
                                                <thead>
                                                    <th>Band</th>
                                                    <th>LN</th>
                                                    <th>BI</th>
                                                    <th>EB</th>
                                                    <th>ICBA 10</th>
                                                    <th>TRS</th>
                                                    <th>QA</th>
                                                    <th>OPQM</th>
                                                    <th>S&M</th>
                                                    
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <th >A</th>
                                                        <td >0</td>
                                                        <td >0</td>
                                                        <td >0</td>
                                                        <td >0</td>
                                                        <td >0</td>
                                                        <td >0</td>
                                                        <td >0</td>
                                                        <td >0</td>
                                                    </tr>
                                                    <tr>
                                                    <th >B</th>
                                                        <td >0</td>
                                                        <td >0</td>
                                                        <td >0</td>
                                                        <td >0</td>
                                                        <td >0</td>
                                                        <td >0</td>
                                                        <td >0</td>
                                                        <td >0</td>
                                                    <tr>
                                                    <th >C</th>
                                                        <td >0</td>
                                                        <td >0</td>
                                                        <td >0</td>
                                                        <td >0</td>
                                                        <td >0</td>
                                                        <td >0</td>
                                                        <td >0</td>
                                                        <td >0</td>                                      
                                                    </tr>
                                                    <tr>
                                                    <th >D</th>
                                                        <td >0</td>
                                                        <td >0</td>
                                                        <td >0</td>
                                                        <td >0</td>
                                                        <td >0</td>
                                                        <td >0</td>
                                                        <td >0</td>
                                                        <td >0</td>
                                                    </tr>
                                                    <tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        
                                        
                                    </div>
                                </div>
                                </div>
                                <!-- /.card -->
                            </div>
                        </div><!-- /.evaluation row END -->

                                
                                <!-- /.card -->
                            </div>
                        </div><!-- /.other records row END -->
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection