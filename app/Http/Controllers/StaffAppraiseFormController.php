<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class StaffAppraiseFormController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {   
        return view('content.appraisal.appraise-form');
    }
    public function review()
    {
        return view('content.appraisal.review');
    }
    public function test(Request $request)
    {
        switch($request->button){
            case 'save':
                return back()->with('success', 'Self Appraisal Saved');
            break;            
            case 'submit':
                return redirect()->route('appr-dash')->with('success', 'Self Appraisal Submitted');
            break;
        }
    }
}
