<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGuidelinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('guidelines', function (Blueprint $table) {
            $table->string('guide_id')->unique();
            $table->string('guide_name');
            $table->longText('guide_desc');
            $table->decimal('weightage')->nullable();
            $table->integer('tier');
            $table->string('guide_parent_id')->nullable();
            $table->timestamps();

            $table->primary('guide_id');
            $table->foreign('guide_parent_id')->references('guide_id')->on('guidelines');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('guidelines');
    }
}
