<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEvaluationScoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('evaluation_scores', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('eval_id')->unsigned();
            $table->string('guide_id');
            $table->decimal('score');
            $table->timestamps();

            $table->foreign('eval_id')->references('id')->on('evaluations');
            $table->foreign('guide_id')->references('guide_id')->on('guidelines');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('evaluation_scores');
    }
}
