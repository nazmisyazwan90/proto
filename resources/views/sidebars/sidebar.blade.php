@if (Route::current()->getName() === 'starter')
  @include('sidebars.startersidebar')
@else
  @include('sidebars.general')
@endif