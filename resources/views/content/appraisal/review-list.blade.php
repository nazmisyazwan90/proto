@extends('layouts.mastercontent')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                <h1>Admin Dashboard</h1>
                </div>
                <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active">Blank Page</li>
                </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <section class="col-12">
                <h1 class="mt-2 ml-2 mb-4">Appraisal Review List</h1>
            </section>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <table id="review-list" class="table table-hover table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Joined Date</th>
                                <th>Department</th>
                                <th>Position</th>
                                <th>Appraiser</th>
                                <th>Appraisal Status</th>
                                <th>Review Status</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr class="rowlink">
                                <td><a href="{{ route('review-form') }}">Nazmi Syazwan Bin Mohamad Zin</a></td>
                                <td>22/5/2011</td>
                                <td>Internal</td>
                                <td>Senior System Engineer</td>
                                <td>Abduzarif Madraimov</td>
                                <td>Complete</td>
                                <td>Incomplete</td>
                            </tr>
                            <tr class="rowlink">
                                <td><a href="{{ route('review-form') }}">Wyncent Lee</a></td>
                                <td>22/5/2011</td>
                                <td>Internal</td>
                                <td>Software Engineer</td>
                                <td>Raynald Vun</td>
                                <td>Complete</td>
                                <td>Reviewed</td>
                            </tr>
                            <tr class="rowlink">
                                <td><a href="{{ route('review-form') }}">Karim Benzema</a></td>
                                <td>14/09/2009</td>
                                <td>Internal</td>
                                <td>System Engineer</td>
                                <td>Ganesh</td>
                                <td>Complete</td>
                                <td>Reviewed</td>
                            </tr>
                            <tr class="rowlink">
                                <td><a href="{{ route('review-form') }}">Ghanin</a></td>
                                <td>14/02/2009</td>
                                <td>Internal</td>
                                <td>System Tester</td>
                                <td>Sharan Segaran</td>
                                <td>Complete</td>
                                <td>Incomplete</td>
                            </tr>
                            <tr class="rowlink">
                                <td><a href="{{ route('review-form') }}">Nick Nonis</a></td>
                                <td>21/02/2006</td>
                                <td>Internal</td>
                                <td>System Desinger</td>
                                <td>Sharan Segaran</td>
                                <td>Incomplete</td>
                                <td>Incomplete</td>
                            </tr>
                            <tr class="rowlink">
                                <td><a href="{{ route('review-form') }}">Samy Velu</a></td>
                                <td>12/12/2012</td>
                                <td>Internal</td>
                                <td>System Developer</td>
                                <td>Abduzarif Madraimov</td>
                                <td>Incomplete</td>
                                <td>Incomplete</td>
                            </tr>
                            <tr class="rowlink">
                                <td><a href="{{ route('review-form') }}">Arief Anwar</a></td>
                                <td>29/05/2008</td>
                                <td>Internal</td>
                                <td>Junior Software Engineer</td>
                                <td>Raynald Vun</td>
                                <td>Incomplete</td>
                                <td>Incomplete</td>
                            </tr>
                            <tr class="rowlink">
                                <td><a href="{{ route('review-form') }}">Izzatul</a></td>
                                <td>22/5/2011</td>
                                <td>Internal</td>
                                <td>System Tester</td>
                                <td>Ganesh</td>
                                <td>Incomplete</td>
                                <td>Incomplete</td>
                            </tr>
                            <tr class="rowlink">
                                <td><a href="{{ route('review-form') }}">Imran</a></td>
                                <td>13/04/1998</td>
                                <td>Internal</td>
                                <td>System Designer</td>
                                <td>Raynald Vun</td>
                                <td>Incomplete</td>
                                <td>Incomplete</td>
                            </tr>
                            <tr class="rowlink">
                                <td><a href="{{ route('review-form') }}">Su Khei Lun</a></td>
                                <td>22/5/2011</td>
                                <td>Internal</td>
                                <td>System Engineer</td>
                                <td>Sharan</td>
                                <td>Incomplete</td>
                                <td>Incomplete</td>
                            </tr>
                            <tr class="rowlink">
                                <td><a href="{{ route('review-form') }}">Priya</a></td>
                                <td>22/5/2011</td>
                                <td>Internal</td>
                                <td>System Engineer</td>
                                <td>Abduzarif Madraimov</td>
                                <td>Incomplete</td>
                                <td>Incomplete</td>
                            </tr>
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>Name</th>
                                <th>Joined Date</th>
                                <th>Department</th>
                                <th>Position</th>
                                <th>Appraiser</th>
                                <th>Appraisal Status</th>
                                <th>Review Status</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div> 
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection