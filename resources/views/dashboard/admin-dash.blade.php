@extends('layouts.mastercontent')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
            <h1>Admin Dashboard</h1>
            </div>
            <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item active">Blank Page</li>
            </ol>
            </div>
        </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <section class="col-12">
                <h1 class="mt-2 ml-2 mb-4">Hello, Sharan!</h1>
            </section>
        </div>
        <div class="row">
            <section class="col-7">
                <div class="row">
                    <section class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">My Details</h3>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <section class="col-3 text-center">
                                        - Picture Here -
                                    </section>
                                    <section class="col-9">
                                        <p class="mb-3"><span class="mr-2" style="font-weight: bold">Name:</span>Sharan Segaran<br></p>
                                        <p class="mb-3"><span class="mr-2" style="font-weight: bold">E-mail:</span>ss97@gmail.com</p>
                                        <p class="mb-3"><span class="mr-2" style="font-weight: bold">Department:</span>Internal</p>
                                        <p class="mb-3"><span class="mr-2" style="font-weight: bold">Position:</span>Senior Administrative</p>
                                        <p class="mb-3"><span class="mr-2" style="font-weight: bold">Joined Date:</span>16 January 2009</p>
                                        <p class="mb-3"><span class="mr-2" style="font-weight: bold">Last Promoted:</span>15 June 2013</p>
                                    </section>
                                </div>
                            </div>
                        </div>           
                    </section>
                </div>        
            </section>
            <section class="col-5 align-content-right">
                <a class="btn btn-block btn-primary btn-lg" style="background-color: darkorange; color: white;" href="{{ route('appraise-list') }}">Appraise Staff</a>
                <a class="btn btn-block btn-warning btn-lg" style="background-color: purple; color: white;" href="{{ route('review-list') }}">Review Appraisal</a>
                <a class="btn btn-block btn-secondary btn-lg" style="background-color:darkgreen; color: white;" href="{{ route('guideline') }}">View Guidelines</a>
                <a class="btn btn-block btn-secondary btn-lg" style="background-color:saddlebrown; color: white;" href="{{ route('admin-details') }}">Appraisal Info</a>
            </section>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection