<li class="nav-item has-treeview {!! classActivePath(1,'charts') !!}">
    <a href="#" class="nav-link {!! classActiveSegment(1, 'charts') !!}">
      <i class="nav-icon fas fa-chart-pie"></i>
      <p>
        Charts
        <i class="right fa fa-angle-left"></i>
      </p>
    </a>
    <ul class="nav nav-treeview">
        <li class="nav-item">
            <a href={{ url('charts/chartjs') }} class="nav-link {!! classActiveSegment(2, 'chartjs') !!} treeview-first">
              <i class="fa fa-circle-o nav-icon"></i>
              <p>ChartJS</p>
            </a>
        </li>
        <li class="nav-item">
            <a href={{ url('charts/flot') }} class="nav-link {!! classActiveSegment(2, 'flot') !!} treeview-first">
              <i class="fa fa-circle-o nav-icon"></i>
              <p>Flot</p>
            </a>
        </li>
        <li class="nav-item">
            <a href={{ url('/charts/inline') }} class="nav-link {!! classActiveSegment(2, 'inline') !!} treeview-first">
              <i class="fa fa-circle-o nav-icon"></i>
              <p>Inline</p>
            </a>
        </li>
    </ul>
</li>