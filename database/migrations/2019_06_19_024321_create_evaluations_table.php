<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEvaluationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('evaluations', function (Blueprint $table) {
            $table->increments('id');
            $table->date('eval_date');
            $table->integer('staff_id')->unsigned();
            $table->integer('appraiser_id')->unsigned();
            $table->integer('reviewer_id')->unsigned();
            $table->decimal('overall_score');
            $table->string('perf_grade');
            $table->boolean('promotion_rec');
            $table->string('promotion_pos');
            $table->date('review_date')->nullable();
            $table->longText('eval_remarks')->nullable();
            $table->longText('self_accomplishment');
            $table->longText('self_str');
            $table->longText('self_improve');
            $table->longText('self_future');
            $table->timestamps();

            $table->foreign('staff_id')->references('id')->on('users');
            $table->foreign('appraiser_id')->references('id')->on('users');
            $table->foreign('reviewer_id')->references('id')->on('users');

            $table->foreign('promotion_pos')->references('pos_id')->on('positions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('evaluations');
    }
}
