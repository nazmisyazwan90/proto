<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ContentController extends Controller
{
    //main content
    public function showwidgetpage(){
        return view('content.widgets');
    }

    public function showcalendarpage(){
        return view('content.calendar');
    }

    //charts
    public function showchartjspage(){
        return view('content.charts.chartjs');
    }

    public function showflotpage(){
        return view('content.charts.flot');
    }

    public function showinlinepage(){
        return view('content.charts.inline');
    }

    //ui elements
    public function showgeneralpage(){
        return view('content.ui.general');
    }

    public function showiconpage(){
        return view('content.ui.icons');
    }

    public function showbuttonspage(){
        return view('content.ui.buttons');
    }

    public function showsliderspage(){
        return view('content.ui.sliders');
    }

    //forms
    public function showgeneralelementspage(){
        return view('content.forms.generalelements');
    }

    public function showadvancedpage(){
        return view('content.forms.advanced');
    }

    public function showeditorspage(){
        return view('content.forms.editors');
    }

    //tables
    public function showsimpletablespage(){
        return view('content.tables.simpletables');
    }

    public function showdatatablespage(){
        return view('content.tables.datatables');
    }

    //mail
    public function showinboxpage(){
        return view('content.mailbox.inbox');
    }

    public function showcomposepage(){
        return view('content.mailbox.compose');
    }

    public function showreadmailpage(){
        return view('content.mailbox.read');
    }

    //pages
    public function showinvoicepage(){
        return view('content.pages.invoice');
    }

    public function showinvoiceprint(){
        return view('content.print.invoice');
    }

    public function showprofilepage(){
        return view('content.pages.profile');
    }

    public function showlockscreenpage(){
        return view('content.pages.lockscreen');
    }

    //extras
    public function showstarterpage(){
        return view('content.extras.starter');
    }

    public function show404page(){
        return view('content.extras.404');
    }

    public function show500page(){
        return view('content.extras.500');
    }

    public function showblankpage(){
        return view('content.extras.blankpage');
    }

    //test

    public function showtestpage(){
        return view('test.test1');
    }
}
