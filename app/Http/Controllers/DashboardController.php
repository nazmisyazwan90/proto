<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function versionone()
    {
        return view('dashboard.v1');
    }
    public function versiontwo()
    {
        return view('dashboard.v2');
    }
    public function versionthree()
    {
        return view('dashboard.v3');
    }
    public function admin_dash()
    {
        return view('dashboard.admin-dash');
    }

    public function appr_dash()
    {
        return view('dashboard.appr-dash');
    }

    public function review_dash()
    {
        return view('dashboard.review-dash');
    }
}
