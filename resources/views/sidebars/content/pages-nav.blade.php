<li class="nav-item has-treeview {!! classActivePath(1, 'pages') !!}">
    <a href="#" class="nav-link {!! classActiveSegment(1, 'pages') !!}">
      <i class="nav-icon fa fa-book"></i>
      <p>
        Pages
        <i class="fa fa-angle-left right"></i>
      </p>
    </a>
    <ul class="nav nav-treeview">
        <li class="nav-item">
            <a href={{ url('/pages/invoice') }} class="nav-link {!! classActiveSegment(2, 'invoice') !!} treeview-first">
                <i class="fa fa-circle-o nav-icon"></i>
                <p>Invoice</p>
            </a>
        </li>
        <li class="nav-item">
            <a href={{ url('/pages/profile') }} class="nav-link {!! classActiveSegment(2, 'profile') !!} treeview-first">
                <i class="fa fa-circle-o nav-icon"></i>
                <p>Profile</p>
            </a>
        </li>
        <li class="nav-item">
            <a href={{ url('/pages/lockscreen') }} class="nav-link {!! classActiveSegment(2, 'lockscreen') !!} treeview-first">
                <i class="fa fa-circle-o nav-icon"></i>
                <p>Lockscreen</p>
            </a>
        </li>
    </ul>
</li>