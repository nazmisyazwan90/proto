  <!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4 ie-scrollbar-hide">
  
  @include('sidebars.includes.logo')
  <!-- Sidebar -->
  <div class="sidebar">
    @include('sidebars.includes.user')
    <!-- Sidebar Menu -->
    <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          @include('sidebars.content.staff-nav')
          @include('sidebars.content.appraise-nav')
          @include('sidebars.content.review-nav')
          @include('sidebars.content.admin-nav')
          <div class="divider"><br></div>
<!--  comment out template starts here        
          @include('sidebars.content.dashboard-nav')
          <li class="nav-item">
            <a href={{ url('/widgets') }} class="nav-link {!! classActiveSegment(1, 'widgets') !!}">
              <i class="nav-icon fa fa-th"></i>
              <p>
                Widgets
                <span class="right badge badge-danger">New</span>
              </p>
            </a>
          </li>

          @auth
            @include('sidebars.content.charts-nav')
            @include('sidebars.content.ui-nav')
            @include('sidebars.content.forms-nav')
            @include('sidebars.content.tables-nav')
            
            <li class="nav-header">EXAMPLES</li>
            <li class="nav-item">
              <a href={{ url('/calendar') }} class="nav-link {!! classActiveSegment(1, 'calendar') !!}">
                <i class="nav-icon fa fa-calendar"></i>
                <p>
                  Calendar
                  <span class="badge badge-info right">2</span>
                </p>
              </a>
            </li>
            @include('sidebars.content.mail-nav')
            @include('sidebars.content.pages-nav')
            @include('sidebars.content.extras-nav')

            <li class="nav-header">MISCELLANEOUS</li>
            @include('sidebars.content.doc-nav')

            <li class="nav-header">LABELS</li>
            <li class="nav-item">
              <a href="#" class="nav-link">
                <i class="nav-icon fa fas fa-circle-notch text-danger"></i>
                <p class="text">Important</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="#" class="nav-link">
                <i class="nav-icon fa fas fa-circle-notch text-warning"></i>
                <p>Warning</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="#" class="nav-link">
                <i class="nav-icon fa fas fa-circle-notch text-info"></i>
                <p>Informational</p>
              </a>
            </li>
          @endauth
commented template elements ENDS here -->
        </ul>
    </nav>
    <!-- /.sidebar-menu -->
  </div>
  <!-- /.sidebar -->
</aside>   