<li class="nav-item has-treeview {!! classActivePath(1, 'ui') !!}">
    <a href="#" class="nav-link {!! classActiveSegment(1, 'ui') !!}">
      <i class="nav-icon fa fa-tree"></i>
      <p>
        UI Elements
        <i class="fa fa-angle-left right"></i>
      </p>
    </a>
    <ul class="nav nav-treeview">
        <li class="nav-item">
            <a href={{ url('/ui/general') }} class="nav-link {!! classActiveSegment(2, 'general') !!} treeview-first">
                <i class="fa fa-circle-o nav-icon"></i>
                <p>General</p>
            </a>
        </li>
        <li class="nav-item">
            <a href={{ url('/ui/icons') }} class="nav-link {!! classActiveSegment(2, 'icons') !!} treeview-first">
                <i class="fa fa-circle-o nav-icon"></i>
                <p>Icons</p>
            </a>
        </li>
        <li class="nav-item">
            <a href={{ url('/ui/buttons') }} class="nav-link {!! classActiveSegment(2, 'buttons') !!} treeview-first">
                <i class="fa fa-circle-o nav-icon"></i>
                <p>Buttons</p>
            </a>
        </li>
        <li class="nav-item">
            <a href={{ url('/ui/sliders') }} class="nav-link {!! classActiveSegment(2, 'sliders') !!} treeview-first">
                <i class="fa fa-circle-o nav-icon"></i>
                <p>Sliders</p>
            </a>
        </li>
    </ul>
</li>