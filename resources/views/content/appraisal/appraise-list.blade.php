@extends('layouts.mastercontent')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                <h1>Admin Dashboard</h1>
                </div>
                <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active">Blank Page</li>
                </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <section class="col-12">
                <h1 class="mt-2 ml-2 mb-4">Staff Appraisal List</h1>
            </section>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <table id="appraise-list" class="table table-hover table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Joined Date</th>
                                <th>Department</th>
                                <th>Position</th>
                                <th>Last Promoted</th>
                                <th>Appraisal Status</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr class="rowlink">
                                <td><a href="{{ route('appraise-form') }}">Nazmi Syazwan Bin Mohamad Zin</a></td>
                                <td>22/5/2011</td>
                                <td>Internal</td>
                                <td>Senior System Engineer</td>
                                <td>22/6/2014</td>
                                <td>Complete</td>
                            </tr>
                            <tr class="rowlink">
                                <td><a href="{{ route('appraise-form') }}">Wyncent Lee</a></td>
                                <td>22/5/2011</td>
                                <td>Internal</td>
                                <td>software engineer</td>
                                <td>20/8/2012</td>
                                <td>Complete</td>
                            </tr>
                            <tr class="rowlink">
                                <td><a href="{{ route('appraise-form') }}">Karim benzema</a></td>
                                <td>14/09/2009</td>
                                <td>Internal</td>
                                <td>System Engineer</td>
                                <td>22/7/2014</td>
                                <td>Complete</td>
                            </tr>
                            <tr class="rowlink">
                                <td><a href="{{ route('appraise-form') }}">Ghanin</a></td>
                                <td>14/02/2003</td>
                                <td>Internal</td>
                                <td>System Tester</td>
                                <td>21/09/2018</td>
                                <td>Complete</td>
                            </tr>
                            <tr class="rowlink">
                                <td><a href="{{ route('appraise-form') }}">Nick Nonis</a></td>
                                <td>21/02/2006</td>
                                <td>Internal</td>
                                <td>System Designer</td>
                                <td>29/02/2015</td>
                                <td>Incomplete</td>
                            </tr>
                            <tr class="rowlink">
                                <td><a href="{{ route('appraise-form') }}">Izzatul </a></td>
                                <td>22/5/2011</td>
                                <td>Internal</td>
                                <td>System Tester</td>
                                <td>21/01/2013</td>
                                <td>Incomplete</td>
                            </tr>
                            <tr class="rowlink">
                                <td><a href="{{ route('appraise-form') }}">Priya</a></td>
                                <td>13/04/2001</td>
                                <td>Internal</td>
                                <td>System Tester</td>
                                <td>25/06/2009</td>
                                <td>Incomplete</td>
                            </tr>
                            <tr class="rowlink">
                                <td><a href="{{ route('appraise-form') }}">Samy Velu</a></td>
                                <td>12/12/2012</td>
                                <td>Internal</td>
                                <td>System Developer</td>
                                <td>14/05/2018</td>
                                <td>Incomplete</td>
                            </tr>
                            <tr class="rowlink">
                                <td><a href="{{ route('appraise-form') }}">Arief Anwar</a></td>
                                <td>29/05/2008</td>
                                <td>Internal</td>
                                <td>Junior Software Engineer</td>
                                <td>17/05/2016</td>
                                <td>Complete</td>
                            </tr>
                            <tr class="rowlink">
                                <td><a href="{{ route('appraise-form') }}">Imran</a></td>
                                <td>13/04/1998</td>
                                <td>Internal</td>
                                <td>System Designer</td>
                                <td>06/12/2019</td>
                                <td>Incomplete</td>
                            </tr>
                            <tr class="rowlink">
                                <td><a href="{{ route('appraise-form') }}">Su Khei Lun</a></td>
                                <td>02/5/2001</td>
                                <td>Internal</td>
                                <td>System Analysis</td>
                                <td>16/01/2011</td>
                                <td>Incomplete</td>
                            </tr>
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>Name</th>
                                <th>Joined Date</th>
                                <th>Department</th>
                                <th>Position</th>
                                <th>Last Promoted</th>
                                <th>Appraisal Status</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div> 
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection