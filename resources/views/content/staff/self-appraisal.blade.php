@extends('layouts.mastercontent')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
            <h1>Admin Dashboard</h1>
            </div>
            <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item active">Blank Page</li>
            </ol>
            </div>
        </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        @include('alerts.alerts')<!-- ALERTS -->
        <div class="row">
            <section class="col-12">
                <h1 class="mt-2 ml-2 mb-4">Self Appraisal Form</h1>
            </section>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <form role="form" action="{{ route('self-submit') }}">
                            <div class="form-group row">
                                <label>Accomplishments</label>
                                <textarea class="form-control" rows="4" placeholder="Accomplishments..."></textarea>
                            </div>
                            <div class="form-group row">
                                <label>Strengths & Capabilities</label>
                                <textarea class="form-control" rows="4" placeholder="Strengths & Capabilities..."></textarea>
                            </div>
                            <div class="form-group row">
                                <label>Improvement Opportunities</label>
                                <textarea class="form-control" rows="4" placeholder="Improvement Opportunities..."></textarea>
                            </div>
                            <div class="form-group row">
                                <label>Future Plan</label>
                                <textarea class="form-control" rows="4" placeholder="Future Plan..."></textarea>
                            </div>
                            <div class="row float-right">
                            <button type="submit" name="button" value="save" class="btn btn-success mr-2">Save</button>
                            <button type="submit" name="button" value="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection