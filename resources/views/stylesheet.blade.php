<link rel="stylesheet" href="/css/app.css">
<!-- Font-awesome -->
<link rel="stylesheet" href="/dist/plugins/font-awesome/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">


@switch (Route::current()->getName())
   @case ('sliders')
      <!-- Ion Slider -->
      <link rel="stylesheet" href="/dist/plugins/ionslider/ion.rangeSlider.css">
      <!-- ion slider Nice -->
      <link rel="stylesheet" href="/dist/plugins/ionslider/ion.rangeSlider.skinNice.css">
      <!-- bootstrap slider -->
      <link rel="stylesheet" href="/dist/plugins/bootstrap-slider/slider.css">
      @break
   @case ('advanced')
      <!-- daterange picker -->
      <link rel="stylesheet" href="/dist/plugins/daterangepicker/daterangepicker-bs3.css">
      <!-- iCheck for checkboxes and radio inputs -->
      <link rel="stylesheet" href="/dist/plugins/iCheck/all.css">
      <!-- Bootstrap Color Picker -->
      <link rel="stylesheet" href="/dist/plugins/colorpicker/bootstrap-colorpicker.min.css">
      <!-- Bootstrap time Picker -->
      <link rel="stylesheet" href="/dist/plugins/timepicker/bootstrap-timepicker.min.css">
      <!-- Select2 -->
      <link rel="stylesheet" href="/dist/plugins/select2/select2.min.css">
      @break
   @case ('calendar')
      <!-- fullCalendar 2.2.5-->
      <link rel="stylesheet" href="/dist/plugins/fullcalendar/fullcalendar.min.css">
      <link rel="stylesheet" href="/dist/plugins/fullcalendar/fullcalendar.print.css" media="print">      
      @break
@endswitch

<!-- datetimepicker -->
<link rel="stylesheet" href="/dist/css/datetimepicker/datetimepicker.min.css">
<!-- Theme style -->
<link rel="stylesheet" href="/dist/css/adminlte.min.css">
<!-- Google Font: Source Sans Pro -->
<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
<!-- nanoscroller -->
<link rel="stylesheet" href="/dist/css/nanoscroller/nanoscroller.css">
<!-- Custom CSS -->
<link rel ="stylesheet" href="/dist/css/custom.css">