<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class StaffPastAppraisalController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {   
        return view('content.staff.past-appraisal');
    }
}
