@extends('layouts.master')
@section('newContent')
    <div class="wrapper" id="app">
            <!-- Header -->
        @include('layouts.header')
            <!-- Sidebar -->
        @include('sidebars.sidebar')
            <!-- Content -->
        @yield('content')
            <!-- Footer -->
        @include('layouts.footer')
    </div>
    <!-- wrapper -->
@endsection