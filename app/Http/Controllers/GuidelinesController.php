<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class GuidelinesController extends Controller
{
    public function guideline()
    {
        return view('content.admin.guidelines');
    }

    public function edit()
    {
        return view('content.admin.edit-guideline');
    }

    public function done()
    {
        return redirect()->route('guideline')->with('success', 'Guidelines Saved');
    }
}