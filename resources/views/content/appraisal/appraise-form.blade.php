@extends('layouts.mastercontent')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
            <h1>Admin Dashboard</h1>
            </div>
            <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item active">Blank Page</li>
            </ol>
            </div>
        </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        @include('alerts.alerts')
        <div class="row">
            <section class="col-12">
                <h1 class="mt-2 ml-2 mb-4">Staff Appraisal Form</h1>
            </section>
        </div>
        <div class="row">
            <section class="col-7">
                <div class="row">
                    <section class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Staff Details</h3>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <section class="col-3 text-center">
                                        - Picture Here -
                                    </section>
                                    <section class="col-9">
                                        <p class="mb-3"><span class="mr-2" style="font-weight: bold">Name:</span>Nazmi Syazwan Bin Mohamad Zin<br></p>
                                        <p class="mb-3"><span class="mr-2" style="font-weight: bold">E-mail:</span>nazmisyazwan@hotmail.com</p>
                                        <p class="mb-3"><span class="mr-2" style="font-weight: bold">Department:</span>Internal</p>
                                        <p class="mb-3"><span class="mr-2" style="font-weight: bold">Position:</span>System Engineer</p>
                                        <p class="mb-3"><span class="mr-2" style="font-weight: bold">Joined Date:</span>22 May 2015</p>
                                        <p class="mb-3"><span class="mr-2" style="font-weight: bold">Last Promoted:</span>15 June 2016</p>
                                    </section>
                                </div>
                            </div>
                        </div>           
                    </section>
                </div>        
            </section>
            <section class="col-5 align-content-right">
                <div class="card">
                    <div class="card-body text-center align-content-center">
                        <p>Staff Appraisal Status:</p>
                        <p style="color:red; font-weight:bold">Incomplete</p>
                    </div>
                </div>
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <h4 class="ml-3">Overall Score: <span style="font-weight: bold;">-</span></h4>
                        </div>
                        <div class="row">
                            <h4 class="ml-3">Performance Band: <span style="font-weight: bold;">-</span></h4>
                        </div>
                    </div>
                </div>
            </section>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row mb-2"><!-- /.year row -->
                            <div class="col-1 text-center my-auto" style="font-weight:bold;">
                                <div>Year:</div>
                            </div>
                            <div class="text-center border" style="width:10%">
                                2019
                            </div>
                        </div><!-- /.year row END-->
                        <div class="row mb-2">
                            <div class="col-1 text-center my-auto" style="font-weight:bold;">
                                <div>Appraisal Period:</div>
                            </div>
                            <form class="w-75">
                                <table class="table table-bordered my-auto text-center">
                                    <tr>
                                        <td><input class="text-center" type="text" id="eval_before" placeholder="DD/MM"/></td>
                                        <td>to</td>
                                        <td><input class="text-center" type="text" id="eval_after" placeholder="DD/MM"/></td>
                                    </tr>
                                </table>
                            </form>
                        </div>
                    <form role="form" action="{{ route('self-submit') }}"><!-- /.FORM START -->
                            <div class="row"><!-- /.evaluation row -->
                                <div class="col-12">
                                    <div class="card">
                                        <div class="card-header d-flex p-0">
                                            <h3 class="card-title p-3">Evaluation Table</h3>
                                            <ul class="nav nav-pills ml-auto p-2">
                                                <li class="nav-item dropdown">
                                                    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#">
                                                        Category<span class="caret"></span>
                                                    </a>
                                                    <div class="dropdown-menu">
                                                        <a class="dropdown-item" tabindex="-1" href="#capability" data-toggle="tab">Capability</a>
                                                        <a class="dropdown-item" tabindex="-1" href="#contribution" data-toggle="tab">Contribution</a>
                                                        <a class="dropdown-item" tabindex="-1" href="#attitude" data-toggle="tab">Attitude</a>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                        <!-- /.card-header -->
                                        <div class="card-body">
                                            <div class="tab-content">
                                                <div class="tab-pane active table-responsive p-0" id="capability">
                                                    <h3 class="ml-1">Capability</h3>
                                                    <table class="table table-hover table-striped table-bordered">
                                                        <tr>
                                                            <th 
                                                                class="w-75" data-toggle='popover' data-html="true" data-placement="bottom" data-trigger="hover"
                                                                title='<strong>Job Knowledge</strong>' 
                                                                data-content='
                                                                    <div class="col-12">
                                                                        <div class="row">
                                                                            Evaluate the employee’s jobspecific knowledge and skill as well
                                                                            as his/her learning ability in picking
                                                                            up new knowledge and skills<br>
                                                                            Note: Knowledge & skills cover
                                                                            across Technical, Product and
                                                                            Process aspects.  
                                                                        </div>
                                                                        <hr/>
                                                                        <div class="row">
                                                                            <ul>
                                                                                <li>
                                                                                    Does the employee have the job knowledge required for the
                                                                                    position and is it applied to maintain a high quality of work?
                                                                                </li>
                                                                                <li> 
                                                                                    Does the employee maintain and attempt to expand the job
                                                                                    knowledge that is required of the position?
                                                                                </li>
                                                                                <li>
                                                                                    Is the employee a subject matter expert that is able to provide
                                                                                    advice to other staff?
                                                                                </li>                                                                                        
                                                                                <li>
                                                                                    Is the employee aware of the emerging trends and understand the
                                                                                    impact to company product / process / business?
                                                                                </li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                '>
                                                                Job Knowledge
                                                            </th>
                                                            <td>
                                                                <select class="form-control">
                                                                    <option disabled selected>Grade</option>
                                                                    <option>Not Applicable</option>
                                                                    <option>Excellent</option>
                                                                    <option>Good</option>
                                                                    <option>Average</option>
                                                                    <option>Below Avergae</option>
                                                                    <option>Poor</option>
                                                                </select>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <th class="w-75" data-toggle='popover' data-html="true" data-placement="bottom" data-trigger="hover"
                                                            title='<strong>Quality of Work</strong>' 
                                                            data-content='
                                                                <div class="col-12">
                                                                    <div class="row">
                                                                            Evaluate the employee’s ability to
                                                                            consistently produce outputs with
                                                                            high standard up to expectation,
                                                                            and work independently with little or
                                                                            no supervision.
                                                                    </div>
                                                                    <hr/>
                                                                    <div class="row">
                                                                        <ul>
                                                                            <li>
                                                                                Does the employee demonstrate a high degree of accuracy in
                                                                                performing all job duties?                                                                                    
                                                                            </li>
                                                                            <li> 
                                                                                Does the employee demonstrate strong sense of excellence in
                                                                                daily work consistently?
                                                                            </li>
                                                                            <li>
                                                                                Do the deliverables of this employee meet customers’ /
                                                                                supervisors’ expectation?
                                                                            </li>                                                                                        
                                                                            <li>
                                                                                Does the employee demonstrate an ability to work independently?
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            '>
                                                                Quality of Work
                                                            </th>
                                                            <td>
                                                                <select class="form-control">
                                                                    <option disabled selected>Grade</option>
                                                                    <option>Not Applicable</option>
                                                                    <option>Excellent</option>
                                                                    <option>Good</option>
                                                                    <option>Average</option>
                                                                    <option>Below Avergae</option>
                                                                    <option>Poor</option>
                                                                </select>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <th class="w-75" data-toggle='popover' data-html="true" data-placement="bottom" data-trigger="hover"
                                                            title='<strong>Communication</strong>' 
                                                            data-content='
                                                                <div class="col-12">
                                                                    <div class="row">
                                                                        Evaluate the employee’s ability to
                                                                        express thoughts & ideas clearly,
                                                                        communicate effectively and
                                                                        appropriately to enhance
                                                                        productivity and maintain respectful
                                                                        relationships.
                                                                    </div>
                                                                    <hr/>
                                                                    <div class="row">
                                                                        <ul>
                                                                            <li>
                                                                                Is the employee able to articulate well verbally?                                                                                
                                                                            </li>
                                                                            <li> 
                                                                                Is the employee able to write with good command of English?
                                                                            </li>
                                                                            <li>
                                                                                Is the employee able to interpret customer requirements well and
                                                                                convey the message effectively to other people?
                                                                            </li>                                                                                        
                                                                            <li>
                                                                                Is the employee able to reduce complex communication to
                                                                                fundamental elements to reach appropriate audience?                                                                                    
                                                                            </li>
                                                                            <li>
                                                                                Does the employee reflect tact, diplomacy and persuasiveness
                                                                                needed to perform the requirements of the position?                                                                             
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            '>
                                                                Communication
                                                            </th>
                                                            <td>
                                                                <select class="form-control">
                                                                    <option disabled selected>Grade</option>
                                                                    <option>Not Applicable</option>
                                                                    <option>Excellent</option>
                                                                    <option>Good</option>
                                                                    <option>Average</option>
                                                                    <option>Below Avergae</option>
                                                                    <option>Poor</option>
                                                                </select>
                                                            </td>                                                
                                                        </tr>
                                                        <tr>
                                                            <th class="w-75" data-toggle='popover' data-html="true" data-placement="bottom" data-trigger="hover"
                                                            title='<strong>Leadership / Managing People</strong>' 
                                                            data-content='
                                                                <div class="col-12">
                                                                    <div class="row">
                                                                        Evaluate the employee’s ability to
                                                                        lead, manage, influence and
                                                                        command respect from fellow
                                                                        colleagues. 
                                                                    </div>
                                                                    <hr/>
                                                                    <div class="row">
                                                                        <ul>
                                                                            <li>
                                                                                Can the employee lead a team and manage all levels of people
                                                                                effectively?                                                                                                                                                             
                                                                            </li>
                                                                            <li> 
                                                                                Do other staff consider this employee as a role model?
                                                                            </li>
                                                                            <li>
                                                                                Is the employee able to spearhead a vision/change, motivate and
                                                                                inspire people to deliver results and achieve the vision?
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            '>
                                                                Leadership / Managing People
                                                            </th>
                                                            <td>
                                                                <select class="form-control">
                                                                    <option disabled selected>Grade</option>
                                                                    <option>Not Applicable</option>
                                                                    <option>Excellent</option>
                                                                    <option>Good</option>
                                                                    <option>Average</option>
                                                                    <option>Below Avergae</option>
                                                                    <option>Poor</option>
                                                                </select>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <th class="w-75" data-toggle='popover' data-html="true" data-placement="bottom" data-trigger="hover"
                                                            title='<strong>Judgement / Decision Making</strong>' 
                                                            data-content='
                                                                <div class="col-12">
                                                                    <div class="row">
                                                                        Evaluate the employee’s logical
                                                                        thinking and objectivity in decision
                                                                        making; shows common sense and
                                                                        sound acumen; anticipates
                                                                        consequences of actions.                                                                            
                                                                    </div>
                                                                    <hr/>
                                                                    <div class="row">
                                                                        <ul>
                                                                            <li>
                                                                                Does the employee make proper use of information to identify
                                                                                problems and make sound decisions?
                                                                            </li>
                                                                            <li> 
                                                                                Does the employee demonstrate good judgement and the
                                                                                appropriate amount of effort and tenaciousness in meeting and
                                                                                solving problems encountered in the position?
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            '>
                                                                Judgement / Decision Making
                                                            </th>
                                                            <td>
                                                                <select class="form-control">
                                                                    <option disabled selected>Grade</option>
                                                                    <option>Not Applicable</option>
                                                                    <option>Excellent</option>
                                                                    <option>Good</option>
                                                                    <option>Average</option>
                                                                    <option>Below Avergae</option>
                                                                    <option>Poor</option>
                                                                </select>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <th class="w-75" data-toggle='popover' data-html="true" data-placement="bottom" data-trigger="hover"
                                                            title='<strong>Problem Solving</strong>' 
                                                            data-content='
                                                                <div class="col-12">
                                                                    <div class="row">
                                                                        Evaluate the employee’s ability to
                                                                        identify, process and filter
                                                                        information methodically in order to
                                                                        resolve issues and come up with
                                                                        possible solutions; and involves
                                                                        others as appropriate.
                                                                    </div>  
                                                                    <hr/>
                                                                    <div class="row">
                                                                        <ul>
                                                                            <li>
                                                                                Is the employee resourceful in finding solutions?                                                                             
                                                                            </li>
                                                                            <li> 
                                                                                Does the employee demonstrate sense of urgency?
                                                                            </li>
                                                                            <li>
                                                                                Does the employee know how to make use of problem solving or
                                                                                debugging techniques to eliminate information and nail the root
                                                                                cause? 
                                                                            </li> 
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            '>
                                                                Problem Solving
                                                            </th>
                                                            <td>
                                                                <select class="form-control">
                                                                    <option disabled selected>Grade</option>
                                                                    <option>Not Applicable</option>
                                                                    <option>Excellent</option>
                                                                    <option>Good</option>
                                                                    <option>Average</option>
                                                                    <option>Below Avergae</option>
                                                                    <option>Poor</option>
                                                                </select>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <div class="tab-pane table-responsive p-0" id="contribution">
                                                    <h3 class="ml-1">Contribution</h3>
                                                    <table class="table table-hover table-striped table-bordered">
                                                        <tr>
                                                            <th class="w-75" data-toggle='popover' data-html="true" data-placement="bottom" data-trigger="hover"
                                                            title='<strong>Efficiency / Multitasking</strong>' 
                                                            data-content='
                                                                <div class="col-12">
                                                                    <div class="row">
                                                                        Evaluate whether the employee
                                                                        accomplishes work in ways that
                                                                        maximise available resources and
                                                                        minimise waste.
                                                                    </div>
                                                                    <hr/>
                                                                    <div class="row">
                                                                        <ul>
                                                                            <li>
                                                                                Was the employee productive all the time?                                                                                  
                                                                            </li>
                                                                            <li> 
                                                                                Did the employee meet deadlines all the time?
                                                                            </li>
                                                                            <li>
                                                                                Did the employee demonstrate good planning and organisational
                                                                                skills?                                                                                    
                                                                            </li>                                                                                        
                                                                            <li>
                                                                                Did the employee make proper use of time to prioritise his/her
                                                                                work to meet expectation?
                                                                            </li>
                                                                            <li>
                                                                                Did the employee make an effort to use resources (such as tools /
                                                                                equipment / manpower) cost effectively?
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            '>
                                                                Efficiency / Multitasking
                                                            </th>
                                                            <td>
                                                                <select class="form-control">
                                                                    <option disabled selected>Grade</option>
                                                                    <option>Not Applicable</option>
                                                                    <option>Excellent</option>
                                                                    <option>Good</option>
                                                                    <option>Average</option>
                                                                    <option>Below Avergae</option>
                                                                    <option>Poor</option>
                                                                </select>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <th class="w-75" data-toggle='popover' data-html="true" data-placement="bottom" data-trigger="hover"
                                                            title='<strong>Effectiveness / Result Driven</strong>' 
                                                            data-content='
                                                                <div class="col-12">
                                                                    <div class="row">
                                                                        Evaluate whether the employee
                                                                        exhibits strong desire to make
                                                                        things happen and is effective in
                                                                        producing results.                                                                            
                                                                    </div>
                                                                    <hr/>
                                                                    <div class="row">
                                                                        <ul>
                                                                            <li>
                                                                                Did the employee strive to work hard and work smart to ensure
                                                                                results are achieved in the end?                                                                                  
                                                                            </li>
                                                                            <li> 
                                                                                Did the employee understand the goals / objectives and begin with
                                                                                an end in mind?                                                                                    
                                                                            </li>
                                                                            <li>
                                                                                Did the employee generate return on investment?                                               
                                                                            </li>                                                                                        
                                                                            <li>
                                                                                Did the employee work well under pressure?
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            '>Effectiveness / Result Driven</th>
                                                            <td>
                                                                <select class="form-control">
                                                                    <option disabled selected>Grade</option>
                                                                    <option>Not Applicable</option>
                                                                    <option>Excellent</option>
                                                                    <option>Good</option>
                                                                    <option>Average</option>
                                                                    <option>Below Avergae</option>
                                                                    <option>Poor</option>
                                                                </select>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <th class="w-75" data-toggle='popover' data-html="true" data-placement="bottom" data-trigger="hover"
                                                            title='<strong>Project KPI Achievement</strong>' 
                                                            data-content='
                                                                <div class="col-12">
                                                                    <div class="row">
                                                                        Measure the performance of the
                                                                        employee’s deliverables based on
                                                                        quantitative KPI targets set.                                                                            
                                                                    </div>
                                                                    <hr/>
                                                                    <div class="row">
                                                                        <ul>
                                                                            <li>
                                                                                Did the employee meet all the timelines when delivering his/her
                                                                                work?                                                                                 
                                                                            </li>
                                                                            <li> 
                                                                                Did the employee complete all his/her work within the budgeted
                                                                                effort?                                                                                                                                                                       
                                                                            </li>
                                                                            <li>
                                                                                Were all the deliverables having minimal or no defect?                                              
                                                                            </li>                                                                                        
                                                                            <li>
                                                                                Did the employee meet SLA or has fast turnaround time
                                                                                consistently?                                                                                    
                                                                            </li>
                                                                            <li>
                                                                                Did the employee comply to QMS processes all the time? 
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            '>Project KPI Achievement</th>
                                                            <td>
                                                                <select class="form-control">
                                                                    <option disabled selected>Grade</option>
                                                                    <option>Not Applicable</option>
                                                                    <option>Excellent</option>
                                                                    <option>Good</option>
                                                                    <option>Average</option>
                                                                    <option>Below Avergae</option>
                                                                    <option>Poor</option>
                                                                </select>
                                                            </td>                                               
                                                        </tr>
                                                        <tr>
                                                            <th class="w-75" data-toggle='popover' data-html="true" data-placement="bottom" data-trigger="hover"
                                                            title='<strong>Customer Oriented / Customer Satisfaction</strong>' 
                                                            data-content='
                                                                <div class="col-12">
                                                                    <div class="row">
                                                                        Evaluate whether the employee
                                                                        strives for excellence in servicing
                                                                        customers, fulfill commitments and
                                                                        contributed to customer satisfaction.                                                                           
                                                                    </div>
                                                                    <hr/>
                                                                    <div class="row">
                                                                        <ul>
                                                                            <li>
                                                                                Did the employee always put customer first when approach work?                                                                               
                                                                            </li>
                                                                            <li> 
                                                                                Did the emploDid the employee demonstrate customer handling skills?                                                                                                                                                          
                                                                            </li>
                                                                            <li>
                                                                                Were customers satisfied with this employee’s work and services?
                                                                            </li>                                                                                        
                                                                            <li>
                                                                                Was the employee able to maintain good rapport with the
                                                                                customers?                                                                                                                                                                    
                                                                            </li>
                                                                            <li>
                                                                                Did the employee reflect tact and diplomacy in handling conflict /
                                                                                dispute with customers? 
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            '>Customer Oriented / Customer Satisfaction</th>
                                                            <td>
                                                                <select class="form-control">
                                                                    <option disabled selected>Grade</option>
                                                                    <option>Not Applicable</option>
                                                                    <option>Excellent</option>
                                                                    <option>Good</option>
                                                                    <option>Average</option>
                                                                    <option>Below Avergae</option>
                                                                    <option>Poor</option>
                                                                </select>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <th class="w-75" data-toggle='popover' data-html="true" data-placement="bottom" data-trigger="hover"
                                                            title='<strong>Value Add / Innovation</strong>' 
                                                            data-content='
                                                                <div class="col-12">
                                                                    <div class="row">
                                                                        Evaluate whether the employee
                                                                        adds value to improvements of the
                                                                        product / process / organisation and
                                                                        introduce new ideas / concepts /
                                                                        strategies.                                                                          
                                                                    </div>
                                                                    <hr/>
                                                                    <div class="row">
                                                                        <ul>
                                                                            <li>
                                                                                Did the employee demonstrate creativity in problem solving?                                                                           
                                                                            </li>
                                                                            <li> 
                                                                                Did the employee show initiative to improve his/her work
                                                                                performance and productivity?                                                                                                                                                                                                                                             
                                                                            </li>
                                                                            <li>
                                                                                Did the employee explore new ideas and methods to reach
                                                                                outcomes?
                                                                            </li>                                                                                        
                                                                            <li>
                                                                                Was the employee able to think out of the box?                                                                                                                                                                    
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            '>Value Add / Innovation</th>
                                                            <td>
                                                                <select class="form-control">
                                                                    <option disabled selected>Grade</option>
                                                                    <option>Not Applicable</option>
                                                                    <option>Excellent</option>
                                                                    <option>Good</option>
                                                                    <option>Average</option>
                                                                    <option>Below Avergae</option>
                                                                    <option>Poor</option>
                                                                </select>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <th class="w-75" data-toggle='popover' data-html="true" data-placement="bottom" data-trigger="hover"
                                                            title='<strong>Integrity / Ethics / Professionalism</strong>' 
                                                            data-content='
                                                                <div class="col-12">
                                                                    <div class="row">
                                                                        Evaluate whether the employee
                                                                        adheres to high standards of
                                                                        personal and professional conduct.                                                                          
                                                                    </div>
                                                                    <hr/>
                                                                    <div class="row">
                                                                        <ul>
                                                                            <li>
                                                                                Did the employee protect company intellectual property and
                                                                                company interest all the time?                                                                        
                                                                            </li>
                                                                            <li> 
                                                                                Did the employee maintain confidentiality of clientele’s
                                                                                information?                                                                                                                                                                                                                                            
                                                                            </li>
                                                                            <li>
                                                                                Was the employee trustworthy and honest?
                                                                            </li>                                                                                        
                                                                            <li>
                                                                                Did the employee exhibit professionalism and code of ethics in
                                                                                handling work / supervisors / coworkers / customers?                                                                                                                                                                                                                                                       
                                                                            </li>
                                                                            <li>
                                                                                Did the employee take shortcut in his/her job duties that caused
                                                                                product quality to be compromised? 
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            '>Integrity / Ethics / Professionalism</th>
                                                            <td>
                                                                <select class="form-control">
                                                                    <option disabled selected>Grade</option>
                                                                    <option>Not Applicable</option>
                                                                    <option>Excellent</option>
                                                                    <option>Good</option>
                                                                    <option>Average</option>
                                                                    <option>Below Avergae</option>
                                                                    <option>Poor</option>
                                                                </select>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <div class="tab-pane table-responsive p-0" id="attitude">
                                                    <h3 class="ml-1">Attitude</h3>
                                                    <table class="table table-hover table-striped table-bordered">
                                                        <tr >
                                                            <th class="w-75" data-toggle='popover' data-html="true" data-placement="bottom" data-trigger="hover"
                                                            title='<strong>Teamwork</strong>' 
                                                            data-content='
                                                                <div class="col-12">
                                                                    <div class="row">
                                                                        Evaluate the employee’s ability to
                                                                        work with others, interacts
                                                                        effectively and builds respectful
                                                                        relationships among coworkers                                                                           
                                                                    </div>
                                                                    <hr/>
                                                                    <div class="row">
                                                                        <ul>
                                                                            <li>
                                                                                Does the employee exhibit cooperation in working with others?                                                                                 
                                                                            </li>
                                                                            <li> 
                                                                                Does the employee offer support to others when needed and help
                                                                                to promote a harmonious working environment?                                                                                                                                                               
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            '>Teamwork</th>
                                                            <td>
                                                                <select class="form-control">
                                                                    <option disabled selected>Grade</option>
                                                                    <option>Not Applicable</option>
                                                                    <option>Excellent</option>
                                                                    <option>Good</option>
                                                                    <option>Average</option>
                                                                    <option>Below Avergae</option>
                                                                    <option>Poor</option>
                                                                </select>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <th class="w-75" data-toggle='popover' data-html="true" data-placement="bottom" data-trigger="hover"
                                                            title='<strong>Diligence</strong>' 
                                                            data-content='
                                                                <div class="col-12">
                                                                    <div class="row">
                                                                        Evaluate the employee’s constant
                                                                        and earnest effort to accomplish
                                                                        what is undertaken.                                                                      
                                                                    </div>
                                                                    <hr/>
                                                                    <div class="row">
                                                                        <ul>
                                                                            <li>
                                                                                Is the employee hardworking?                                                                               
                                                                            </li>
                                                                            <li> 
                                                                                Does the employee always put in concerted effort to accomplish
                                                                                his/her assignments?                                                                                                                                                            
                                                                            </li>
                                                                            <li>
                                                                                Does the employee demonstrate perseverance in overcoming
                                                                                difficult challenges?
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            '>Diligence</th>
                                                            <td>
                                                                <select class="form-control">
                                                                    <option disabled selected>Grade</option>
                                                                    <option>Not Applicable</option>
                                                                    <option>Excellent</option>
                                                                    <option>Good</option>
                                                                    <option>Average</option>
                                                                    <option>Below Avergae</option>
                                                                    <option>Poor</option>
                                                                </select>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <th class="w-75" data-toggle='popover' data-html="true" data-placement="bottom" data-trigger="hover"
                                                            title='<strong>Flexibility</strong>' 
                                                            data-content='
                                                                <div class="col-12">
                                                                    <div class="row">
                                                                        Evaluate the employee’s ability to
                                                                        work with different personalities,
                                                                        different environments, respond
                                                                        positively to changes and adjust
                                                                        quickly to volatility.                                                                  
                                                                    </div>
                                                                    <hr/>
                                                                    <div class="row">
                                                                        <ul>
                                                                            <li>
                                                                                Does the employee exhibit flexibility in working with others?                                                                               
                                                                            </li>
                                                                            <li> 
                                                                                Is the employee able to adapt to changes in work requirements,
                                                                                work environment, people, process, workload and deadlines?                                                                                                                                                           
                                                                            </li>
                                                                            <li>
                                                                                Does the employee show willingness to learn new knowledge and
                                                                                skills?                                                                                    
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            '>Flexibility</th>
                                                            <td>
                                                                <select class="form-control">
                                                                    <option disabled selected>Grade</option>
                                                                    <option>Not Applicable</option>
                                                                    <option>Excellent</option>
                                                                    <option>Good</option>
                                                                    <option>Average</option>
                                                                    <option>Below Avergae</option>
                                                                    <option>Poor</option>
                                                                </select>
                                                            </td>                                               
                                                        </tr>
                                                        <tr>
                                                            <th class="w-75" data-toggle='popover' data-html="true" data-placement="bottom" data-trigger="hover"
                                                            title='<strong>Responsible</strong>' 
                                                            data-content='
                                                                <div class="col-12">
                                                                    <div class="row">
                                                                        Evaluate the employee’s
                                                                        dependability, trustworthiness, and
                                                                        consistency in keeping
                                                                        commitments.                                                                 
                                                                    </div>
                                                                    <hr/>
                                                                    <div class="row">
                                                                        <ul>
                                                                            <li>
                                                                                Is the employee a responsible and reliable person?                                                                               
                                                                            </li>
                                                                            <li> 
                                                                                Does the employee take ownership of his/her work and
                                                                                responsibilities?                                                                                                                                                                                                                                             
                                                                            </li>
                                                                            <li>
                                                                                Does the employee admit his/her own mistakes and never give
                                                                                excuses or blame other people for own problem?                                                                                                                                                                       
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            '>Responsible</th>
                                                            <td>
                                                                <select class="form-control">
                                                                    <option disabled selected>Grade</option>
                                                                    <option>Not Applicable</option>
                                                                    <option>Excellent</option>
                                                                    <option>Good</option>
                                                                    <option>Average</option>
                                                                    <option>Below Avergae</option>
                                                                    <option>Poor</option>
                                                                </select>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <th class="w-75" data-toggle='popover' data-html="true" data-placement="bottom" data-trigger="hover"
                                                            title='<strong>Self-Initiative</strong>' 
                                                            data-content='
                                                                <div class="col-12">
                                                                    <div class="row">
                                                                        Evaluate the employee’s motivation
                                                                        to act without having to be directed,
                                                                        being proactive instead of reactive
                                                                        in accepting additional
                                                                        responsibilities.                                                                
                                                                    </div>
                                                                    <hr/>
                                                                    <div class="row">
                                                                        <ul>
                                                                            <li>
                                                                                Does the employee invite responsibility and perform function
                                                                                beyond the call of duty?                                                                               
                                                                            </li>
                                                                            <li> 
                                                                                Does the employee take ownership of problems including those
                                                                                not specifically assigned to him/her?                                                                                                                                                                                                                                   
                                                                            </li>
                                                                            <li>
                                                                                Is the employee often willing to go the extra mile to deliver his/her
                                                                                work and to help other colleagues?                                                                                                                                                                      
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            '>Self-Initiative</th>
                                                            <td>
                                                                <select class="form-control">
                                                                    <option disabled selected>Grade</option>
                                                                    <option>Not Applicable</option>
                                                                    <option>Excellent</option>
                                                                    <option>Good</option>
                                                                    <option>Average</option>
                                                                    <option>Below Avergae</option>
                                                                    <option>Poor</option>
                                                                </select>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <th class="w-75" data-toggle='popover' data-html="true" data-placement="bottom" data-trigger="hover"
                                                            title='<strong>Discipline</strong>' 
                                                            data-content='
                                                                <div class="col-12">
                                                                    <div class="row">
                                                                        Evaluate the employee’s
                                                                        acceptance of management,
                                                                        policies, and department
                                                                        procedures.                                                             
                                                                    </div>
                                                                    <hr/>
                                                                    <div class="row">
                                                                        <ul>
                                                                            <li>
                                                                                Does the employee accept the authority of management?                                                                               
                                                                            </li>
                                                                            <li> 
                                                                                Does the employee follow and abide by company policies and or
                                                                                department procedures?                                                                                                                                                                                                                                        
                                                                            </li>
                                                                            <li>
                                                                                Is the employee obedient in following supervisor’s instructions?                                                                                                                                                                     
                                                                            </li>
                                                                            <li>
                                                                                Does the employee observe the attendance and punctuality rules
                                                                                set by the company? 
                                                                            </li>
                                                                            <li>
                                                                                Does the employee enter effort data consistently and accurately
                                                                                into TLS?
                                                                            </li>
                                                                            <li>
                                                                                Does the employee enter defect data consistently and accurately
                                                                                into DSSi?
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            '>Discipline</th>
                                                            <td>
                                                                <select class="form-control">
                                                                    <option disabled selected>Grade</option>
                                                                    <option>Not Applicable</option>
                                                                    <option>Excellent</option>
                                                                    <option>Good</option>
                                                                    <option>Average</option>
                                                                    <option>Below Avergae</option>
                                                                    <option>Poor</option>
                                                                </select>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /.card-body -->
                                        <div class="card-footer">
                                            <div class="row">
                                                <div class="col-3 text-center my-auto" style="font-weight:bold;">
                                                    <div>Recommend for Promotion:</div>
                                                </div>
                                                <div class="w-auto">
                                                    <select class="form-control">
                                                        <option>Yes</option>
                                                        <option selected>No</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.card -->
                                </div>
                            </div><!-- /.evaluation row END -->
                            <div class="row"><!-- /.other records row -->
                                <div class="col-12">
                                    <div class="card">
                                        <div class="card-header d-flex p-0">
                                            <h3 class="card-title p-3">Other Records</h3>
                                            <ul class="nav nav-pills ml-auto p-2">
                                                <li class="nav-item"><a class="nav-link" href="#self-appraisal" data-toggle="tab">Staff Self Appraisal</a></li>
                                                <li class="nav-item"><a class="nav-link ml-1 active" href="#remarks" data-toggle="tab">Remarks</a></li>
                                            </ul>
                                        </div>
                                        <div class="card-body">
                                            <div class="tab-content">
                                                <div class="tab-pane" id="self-appraisal">
                                                    <div class="row">
                                                        <div class="col-6">
                                                            <div class="card">
                                                                <div class="card-header">
                                                                    <h4 class="card-title text-center" style="font-weight: bold">Accomplishments</h4>
                                                                </div>
                                                                <div class="card-body">
                                                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                                                                    Nunc aliquam lacinia purus, sed lobortis est pellentesque vel. 
                                                                    Suspendisse et efficitur justo. Vestibulum gravida varius metus, 
                                                                    cursus sollicitudin est tincidunt et. Sed a libero ut quam molestie 
                                                                    posuere sit amet sed velit. Mauris eget leo gravida, tempus nulla id, 
                                                                    dictum lectus. Etiam aliquam magna consectetur metus dapibus, at pretium dolor volutpat. 
                                                                    Sed ac enim ut lectus porta tristique.
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-6">
                                                            <div class="card">
                                                                <div class="card-header">
                                                                    <h4 class="card-title text-center" style="font-weight: bold">Strengths / Capabilities</h4>
                                                                </div>
                                                                <div class="card-body">
                                                                    In eu lectus in dolor rhoncus dictum vitae vitae magna. 
                                                                    Nulla ante est, pulvinar vel justo in, rhoncus hendrerit dui. 
                                                                    Quisque id congue diam. Phasellus aliquet feugiat ultricies. Cras 
                                                                    in efficitur turpis, vulputate maximus magna. Sed egestas nibh diam, 
                                                                    ac mattis velit finibus laoreet. Vestibulum fringilla, neque eu viverra 
                                                                    ornare, dolor tellus posuere ex, eu laoreet magna dui nec neque. Curabitur 
                                                                    auctor, erat in dictum placerat, risus leo semper libero, vel bibendum velit 
                                                                    sapien id augue. Sed feugiat tincidunt sapien. Phasellus eget nisl fermentum, 
                                                                    sollicitudin justo nec, dapibus lectus. Maecenas dignissim magna et enim auctor 
                                                                    rutrum. Praesent viverra gravida ullamcorper.
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-6">
                                                            <div class="card">
                                                                <div class="card-header">
                                                                    <h4 class="card-title text-center" style="font-weight: bold">Improvement Opportunities</h4>
                                                                </div>
                                                                <div class="card-body">
                                                                    Long text here
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-6">
                                                            <div class="card">
                                                                <div class="card-header">
                                                                    <h4 class="card-title text-center" style="font-weight: bold">Future Plan</h4>
                                                                </div>
                                                                <div class="card-body">
                                                                    Long text here
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="tab-pane active" id="remarks">
                                                    <div class="row">
                                                        <div class="col-6">
                                                            <div class="card">
                                                                <div class="card-header" style="background-color:lightgreen">
                                                                    <h4 class="card-title text-center" style="font-weight: bold">Strengths / Potential</h4>
                                                                </div>
                                                                <div class="card-body">
                                                                    <textarea class="form-control" rows="6" placeholder="Strengths / Potential..."></textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-6">
                                                            <div class="card">
                                                                <div class="card-header" style="background-color: lightcoral">
                                                                    <h4 class="card-title text-center" style="font-weight: bold">Weaknesses / Areas of Improvement</h4>
                                                                </div>
                                                                <div class="card-body">
                                                                    <textarea class="form-control" rows="6" placeholder="Weaknesses / Areas of Improvement..."></textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-6">
                                                            <div class="card">
                                                                <div class="card-header" style="background-color:lightskyblue">
                                                                    <h4 class="card-title text-center" style="font-weight: bold">Goals to be Accomplished</h4>
                                                                </div>
                                                                <div class="card-body">
                                                                    <textarea class="form-control" rows="6" placeholder="Goals to be Accomplished..."></textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-6">
                                                            <div class="card">
                                                                <div class="card-header" style="background-color:yellow">
                                                                    <h4 class="card-title text-center" style="font-weight: bold">Other Remarks</h4>
                                                                </div>
                                                                <div class="card-body">
                                                                    <textarea class="form-control" rows="6" placeholder="Other Remarks..."></textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.card -->
                                </div>
                            </div><!-- /.other records row END -->
                            <div class="row">
                                <div class="col-12">
                                    <center>
                                        <button type="submit" class="btn btn-success mr-2" value="save">Save</button>
                                        <button type="submit" class="btn btn-primary" value="submit">Submit</button>
                                    </center>
                                </div>
                            </div>
                        </form><!-- /. form END -->
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection