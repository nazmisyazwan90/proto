<li class="nav-item has-treeview {!! classActivePath(1, 'tables') !!}">
    <a href="#" class="nav-link {!! classActiveSegment(1, 'tables') !!}">
      <i class="nav-icon fa fa-table"></i>
      <p>
        Tables
        <i class="fa fa-angle-left right"></i>
      </p>
    </a>
    <ul class="nav nav-treeview">
        <li class="nav-item">
            <a href={{ url('/tables/simpletables') }} class="nav-link {!! classActiveSegment(2, 'simpletables') !!} treeview-first">
                <i class="fa fa-circle-o nav-icon"></i>
                <p>Simple Tables</p>
            </a>
        </li>
        <li class="nav-item">
            <a href={{ url('/tables/datatables') }} class="nav-link {!! classActiveSegment(2, 'datatables') !!} treeview-first">
                <i class="fa fa-circle-o nav-icon"></i>
                <p>Data Tables</p>
            </a>
        </li>
    </ul>
</li>