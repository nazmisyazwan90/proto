<?php

use Illuminate\Database\Seeder;

class PositionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('positions')->insert([
            'pos_id' => 'P001',
            'pos_name' => 'System Engineer',                    
        ]);

        DB::table('positions')->insert([
            'pos_id' => 'P002',
            'pos_name' => 'Project Manager',                    
        ]);

        DB::table('positions')->insert([
            'pos_id' => 'P003',
            'pos_name' => 'System Consultant',                    
        ]);

        DB::table('positions')->insert([
            'pos_id' => 'P004',
            'pos_name' => 'Vice President',                    
        ]);
    }
}
