<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class StaffDashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function staff_dash()
    {   
        return view('dashboard.staff-dash');
    }
}
