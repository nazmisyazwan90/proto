<li class="nav-item has-treeview {!! classActivePath(1, 'staff') !!}">
    <a href="#" class="nav-link {!! classActiveSegment(1, 'staff') !!}">
      <i class="nav-icon fa fa-book"></i>
      <p>
        Staff
        <i class="fa fa-angle-left right"></i>
      </p>
    </a>
    <ul class="nav nav-treeview">
        <li class="nav-item">
            <a href={{ url('/staff/staff-dash') }} class="nav-link {!! classActiveSegment(2, 'staff-dash') !!} treeview-first">
                <i class="fa fa-circle-o nav-icon"></i>
                <p>Dashboard</p>
            </a>
        </li>
        <li class="nav-item">
            <a href={{ url('/staff/past-appraisal') }} class="nav-link {!! classActiveSegment(2, 'past-appraisal') !!} treeview-first">
                <i class="fa fa-circle-o nav-icon"></i>
                <p>Past Appraisal</p>
            </a>
        </li>
        <li class="nav-item">
            <a href={{ url('/staff/self-appraisal') }} class="nav-link {!! classActiveSegment(2, 'self-appraisal') !!} treeview-first">
                <i class="fa fa-circle-o nav-icon"></i>
                <p>Self Appraisal Form</p>
            </a>
        </li>
    </ul>
</li>