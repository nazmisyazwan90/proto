<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class StaffListController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function appraise_list()
    {   
        return view('content.appraisal.appraise-list');
    }
    public function review_list()
    {   
        return view('content.appraisal.review-list');
    }
}
