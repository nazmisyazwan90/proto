<li class="nav-item has-treeview {!! classActivePath(1, 'admin') !!}">
    <a href="#" class="nav-link {!! classActiveSegment(1, 'admin') !!}">
      <i class="nav-icon fa fa-book"></i>
      <p>
        Admin
        <i class="fa fa-angle-left right"></i>
      </p>
    </a>
    <ul class="nav nav-treeview">
        <li class="nav-item">
            <a href={{ url('/admin/admin-dash') }} class="nav-link {!! classActiveSegment(2, 'admin-dash') !!} treeview-first">
                <i class="fa fa-circle-o nav-icon"></i>
                <p>Dashboard</p>
            </a>
        </li>
        <li class="nav-item">
            <a href={{ url('/admin/guidelines') }} class="nav-link {!! classActiveSegment(2, 'review-list') !!} treeview-first">
                <i class="fa fa-circle-o nav-icon"></i>
                <p>View Guidelines</p>
            </a>
        </li>
        <li class="nav-item">
            <a href={{ url('/admin/edit-guidelines') }} class="nav-link {!! classActiveSegment(2, 'edit-guidelines') !!} treeview-first">
                <i class="fa fa-circle-o nav-icon"></i>
                <p>Edit Guidelines</p>
            </a>
        </li>
        <li class="nav-item">
            <a href={{ url('/admin/admin-details') }} class="nav-link {!! classActiveSegment(2, 'admin-details') !!} treeview-first">
                <i class="fa fa-circle-o nav-icon"></i>
                <p>Details</p>
            </a>
        </li>
    </ul>
</li>