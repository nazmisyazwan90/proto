@extends('layouts.mastercontent')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                <h1>Guidelines</h1>
                </div>
                <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active">Blank Page</li>
                </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        @include('alerts.alerts')
        <div class="row col-12">
            <div class="col-6">
                <h1 class="mt-2 ml-2 mb-4">Guidelines for Evaluation</h1>
            </div>
            <div class="mb-4 mt-3 col-6">
                <a class="btn btn-success float-right" href="{{ route('edit-guideline')}}">Edit Guidelines</a>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header d-flex p-0">
                                        <h3 class="card-title p-3">Competency Evaluation Guidelines</h3>
                                        <ul class="nav nav-pills ml-auto p-2">
                                            <li class="nav-item dropdown">
                                                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#">
                                                    Category<span class="caret"></span>
                                                </a>
                                                <div class="dropdown-menu">
                                                    <a class="dropdown-item" tabindex="-1" href="#capability" data-toggle="tab">Capability</a>
                                                    <a class="dropdown-item" tabindex="-1" href="#contribution" data-toggle="tab">Contribution</a>
                                                    <a class="dropdown-item" tabindex="-1" href="#attitude" data-toggle="tab">Attitude</a>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="card-body">
                                        <div class="tab-content">
                                            <div class="tab-pane active table-responsive p-0" id="capability">
                                                <h3 class="ml-1 pb-2">Capability</h3>
                                                <table class="table table-hover table-striped table-bordered">
                                                    <tr>
                                                        <th></th>
                                                        <th class="text-center">Weightage</th>
                                                    </tr>
                                                    <tr>
                                                        <th class="w-75" data-toggle='popover' data-html="true" data-placement="bottom" data-trigger="hover"
                                                        title='<strong>Job Knowledge</strong>' 
                                                        data-content='
                                                            <div class="col-12">
                                                                <div class="row">
                                                                    Evaluate the employee’s jobspecific knowledge and skill as well
                                                                    as his/her learning ability in picking
                                                                    up new knowledge and skills<br>
                                                                    Note: Knowledge & skills cover
                                                                    across Technical, Product and
                                                                    Process aspects.  
                                                                </div>
                                                                <hr/>
                                                                <div class="row">
                                                                    <ul>
                                                                        <li>
                                                                            Does the employee have the job knowledge required for the
                                                                            position and is it applied to maintain a high quality of work?
                                                                        </li>
                                                                        <li> 
                                                                            Does the employee maintain and attempt to expand the job
                                                                            knowledge that is required of the position?
                                                                        </li>
                                                                        <li>
                                                                            Is the employee a subject matter expert that is able to provide
                                                                            advice to other staff?
                                                                        </li>                                                                                        
                                                                        <li>
                                                                            Is the employee aware of the emerging trends and understand the
                                                                            impact to company product / process / business?
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        '>Job Knowledge</th>
                                                        <td class="text-center">20%</td>
                                                    </tr>
                                                    <tr>
                                                        <th class="w-75" data-toggle='popover' data-html="true" data-placement="bottom" data-trigger="hover"
                                                        title='<strong>Quality of Work</strong>' 
                                                        data-content='
                                                            <div class="col-12">
                                                                <div class="row">
                                                                        Evaluate the employee’s ability to
                                                                        consistently produce outputs with
                                                                        high standard up to expectation,
                                                                        and work independently with little or
                                                                        no supervision.
                                                                </div>
                                                                <hr/>
                                                                <div class="row">
                                                                    <ul>
                                                                        <li>
                                                                            Does the employee demonstrate a high degree of accuracy in
                                                                            performing all job duties?                                                                                    
                                                                        </li>
                                                                        <li> 
                                                                            Does the employee demonstrate strong sense of excellence in
                                                                            daily work consistently?
                                                                        </li>
                                                                        <li>
                                                                            Do the deliverables of this employee meet customers’ /
                                                                            supervisors’ expectation?
                                                                        </li>                                                                                        
                                                                        <li>
                                                                            Does the employee demonstrate an ability to work independently?
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        '>Quality of Work</th>
                                                        <td class="text-center">20%</td>
                                                    </tr>
                                                    <tr>
                                                        <th class="w-75" data-toggle='popover' data-html="true" data-placement="bottom" data-trigger="hover"
                                                        title='<strong>Communication</strong>' 
                                                        data-content='
                                                            <div class="col-12">
                                                                <div class="row">
                                                                    Evaluate the employee’s ability to
                                                                    express thoughts & ideas clearly,
                                                                    communicate effectively and
                                                                    appropriately to enhance
                                                                    productivity and maintain respectful
                                                                    relationships.
                                                                </div>
                                                                <hr/>
                                                                <div class="row">
                                                                    <ul>
                                                                        <li>
                                                                            Is the employee able to articulate well verbally?                                                                                
                                                                        </li>
                                                                        <li> 
                                                                            Is the employee able to write with good command of English?
                                                                        </li>
                                                                        <li>
                                                                            Is the employee able to interpret customer requirements well and
                                                                            convey the message effectively to other people?
                                                                        </li>                                                                                        
                                                                        <li>
                                                                            Is the employee able to reduce complex communication to
                                                                            fundamental elements to reach appropriate audience?                                                                                    
                                                                        </li>
                                                                        <li>
                                                                            Does the employee reflect tact, diplomacy and persuasiveness
                                                                            needed to perform the requirements of the position?                                                                             
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        '>Communication</th>
                                                        <td class="text-center">20%</td>                                                
                                                    </tr>
                                                    <tr>
                                                        <th class="w-75" data-toggle='popover' data-html="true" data-placement="bottom" data-trigger="hover"
                                                        title='<strong>Leadership / Managing People</strong>' 
                                                        data-content='
                                                            <div class="col-12">
                                                                <div class="row">
                                                                    Evaluate the employee’s ability to
                                                                    lead, manage, influence and
                                                                    command respect from fellow
                                                                    colleagues. 
                                                                </div>
                                                                <hr/>
                                                                <div class="row">
                                                                    <ul>
                                                                        <li>
                                                                            Can the employee lead a team and manage all levels of people
                                                                            effectively?                                                                                                                                                             
                                                                        </li>
                                                                        <li> 
                                                                            Do other staff consider this employee as a role model?
                                                                        </li>
                                                                        <li>
                                                                            Is the employee able to spearhead a vision/change, motivate and
                                                                            inspire people to deliver results and achieve the vision?
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        '>Leadership / Managing People</th>
                                                        <td class="text-center">10%</td>
                                                    </tr>
                                                    <tr>
                                                        <th class="w-75" data-toggle='popover' data-html="true" data-placement="bottom" data-trigger="hover"
                                                        title='<strong>Judgement / Decision Making</strong>' 
                                                        data-content='
                                                            <div class="col-12">
                                                                <div class="row">
                                                                    Evaluate the employee’s logical
                                                                    thinking and objectivity in decision
                                                                    making; shows common sense and
                                                                    sound acumen; anticipates
                                                                    consequences of actions.                                                                            
                                                                </div>
                                                                <hr/>
                                                                <div class="row">
                                                                    <ul>
                                                                        <li>
                                                                            Does the employee make proper use of information to identify
                                                                            problems and make sound decisions?
                                                                        </li>
                                                                        <li> 
                                                                            Does the employee demonstrate good judgement and the
                                                                            appropriate amount of effort and tenaciousness in meeting and
                                                                            solving problems encountered in the position?
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        '>Judgement / Decision Making</th>
                                                        <td class="text-center">10%</td>
                                                    </tr>
                                                    <tr>
                                                        <th class="w-75" data-toggle='popover' data-html="true" data-placement="bottom" data-trigger="hover"
                                                        title='<strong>Problem Solving</strong>' 
                                                        data-content='
                                                            <div class="col-12">
                                                                <div class="row">
                                                                    Evaluate the employee’s ability to
                                                                    identify, process and filter
                                                                    information methodically in order to
                                                                    resolve issues and come up with
                                                                    possible solutions; and involves
                                                                    others as appropriate.
                                                                </div>  
                                                                <hr/>
                                                                <div class="row">
                                                                    <ul>
                                                                        <li>
                                                                            Is the employee resourceful in finding solutions?                                                                             
                                                                        </li>
                                                                        <li> 
                                                                            Does the employee demonstrate sense of urgency?
                                                                        </li>
                                                                        <li>
                                                                            Does the employee know how to make use of problem solving or
                                                                            debugging techniques to eliminate information and nail the root
                                                                            cause? 
                                                                        </li> 
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        '>Problem Solving</th>
                                                        <td class="text-center">20%</td>
                                                    </tr>
                                                    <tr>
                                                        <th class="w-50 text-right pr-5">Total Weightage from all Category</th>
                                                        <th class="text-center">40%</th>
                                                    <tr>
                                                </table>
                                            </div>
                                            <div class="tab-pane table-responsive p-0" id="contribution">
                                                <h3 class="ml-1 pb-2">Contribution</h3>
                                                <table class="table table-hover table-striped table-bordered">
                                                    <tr>
                                                        <th></th>
                                                        <th class="text-center">Weightage</th>
                                                    </tr>
                                                    <tr>
                                                        <th class="w-75" data-toggle='popover' data-html="true" data-placement="bottom" data-trigger="hover"
                                                        title='<strong>Efficiency / Multitasking</strong>' 
                                                        data-content='
                                                            <div class="col-12">
                                                                <div class="row">
                                                                    Evaluate whether the employee
                                                                    accomplishes work in ways that
                                                                    maximise available resources and
                                                                    minimise waste.
                                                                </div>
                                                                <hr/>
                                                                <div class="row">
                                                                    <ul>
                                                                        <li>
                                                                            Was the employee productive all the time?                                                                                  
                                                                        </li>
                                                                        <li> 
                                                                            Did the employee meet deadlines all the time?
                                                                        </li>
                                                                        <li>
                                                                            Did the employee demonstrate good planning and organisational
                                                                            skills?                                                                                    
                                                                        </li>                                                                                        
                                                                        <li>
                                                                            Did the employee make proper use of time to prioritise his/her
                                                                            work to meet expectation?
                                                                        </li>
                                                                        <li>
                                                                            Did the employee make an effort to use resources (such as tools /
                                                                            equipment / manpower) cost effectively?
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        '>Efficiency / Multitasking</th>
                                                        <td class="text-center">20%</td>
                                                    </tr>
                                                    <tr>
                                                        <th class="w-75" data-toggle='popover' data-html="true" data-placement="bottom" data-trigger="hover"
                                                        title='<strong>Effectiveness / Result Driven</strong>' 
                                                        data-content='
                                                            <div class="col-12">
                                                                <div class="row">
                                                                    Evaluate whether the employee
                                                                    exhibits strong desire to make
                                                                    things happen and is effective in
                                                                    producing results.                                                                            
                                                                </div>
                                                                <hr/>
                                                                <div class="row">
                                                                    <ul>
                                                                        <li>
                                                                            Did the employee strive to work hard and work smart to ensure
                                                                            results are achieved in the end?                                                                                  
                                                                        </li>
                                                                        <li> 
                                                                            Did the employee understand the goals / objectives and begin with
                                                                            an end in mind?                                                                                    
                                                                        </li>
                                                                        <li>
                                                                            Did the employee generate return on investment?                                               
                                                                        </li>                                                                                        
                                                                        <li>
                                                                            Did the employee work well under pressure?
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        '>Effectiveness / Result Driven</th>
                                                        <td class="text-center">20%</td>
                                                    </tr>
                                                    <tr>
                                                        <th class="w-75" data-toggle='popover' data-html="true" data-placement="bottom" data-trigger="hover"
                                                        title='<strong>Project KPI Achievement</strong>' 
                                                        data-content='
                                                            <div class="col-12">
                                                                <div class="row">
                                                                    Measure the performance of the
                                                                    employee’s deliverables based on
                                                                    quantitative KPI targets set.                                                                            
                                                                </div>
                                                                <hr/>
                                                                <div class="row">
                                                                    <ul>
                                                                        <li>
                                                                            Did the employee meet all the timelines when delivering his/her
                                                                            work?                                                                                 
                                                                        </li>
                                                                        <li> 
                                                                            Did the employee complete all his/her work within the budgeted
                                                                            effort?                                                                                                                                                                       
                                                                        </li>
                                                                        <li>
                                                                            Were all the deliverables having minimal or no defect?                                              
                                                                        </li>                                                                                        
                                                                        <li>
                                                                            Did the employee meet SLA or has fast turnaround time
                                                                            consistently?                                                                                    
                                                                        </li>
                                                                        <li>
                                                                            Did the employee comply to QMS processes all the time? 
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        '>Project KPI Achievement</th>
                                                        <td class="text-center">20%</td>                                                
                                                    </tr>
                                                    <tr>
                                                        <th class="w-75" data-toggle='popover' data-html="true" data-placement="bottom" data-trigger="hover"
                                                        title='<strong>Customer Oriented / Customer Satisfaction</strong>' 
                                                        data-content='
                                                            <div class="col-12">
                                                                <div class="row">
                                                                    Evaluate whether the employee
                                                                    strives for excellence in servicing
                                                                    customers, fulfill commitments and
                                                                    contributed to customer satisfaction.                                                                           
                                                                </div>
                                                                <hr/>
                                                                <div class="row">
                                                                    <ul>
                                                                        <li>
                                                                            Did the employee always put customer first when approach work?                                                                               
                                                                        </li>
                                                                        <li> 
                                                                            Did the emploDid the employee demonstrate customer handling skills?                                                                                                                                                          
                                                                        </li>
                                                                        <li>
                                                                            Were customers satisfied with this employee’s work and services?
                                                                        </li>                                                                                        
                                                                        <li>
                                                                            Was the employee able to maintain good rapport with the
                                                                            customers?                                                                                                                                                                    
                                                                        </li>
                                                                        <li>
                                                                            Did the employee reflect tact and diplomacy in handling conflict /
                                                                            dispute with customers? 
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        '>Customer Oriented / Customer Satisfaction</th>
                                                        <td class="text-center">20%</td>
                                                    </tr>
                                                    <tr>
                                                        <th class="w-75" data-toggle='popover' data-html="true" data-placement="bottom" data-trigger="hover"
                                                        title='<strong>Value Add / Innovation</strong>' 
                                                        data-content='
                                                            <div class="col-12">
                                                                <div class="row">
                                                                    Evaluate whether the employee
                                                                    adds value to improvements of the
                                                                    product / process / organisation and
                                                                    introduce new ideas / concepts /
                                                                    strategies.                                                                          
                                                                </div>
                                                                <hr/>
                                                                <div class="row">
                                                                    <ul>
                                                                        <li>
                                                                            Did the employee demonstrate creativity in problem solving?                                                                           
                                                                        </li>
                                                                        <li> 
                                                                            Did the employee show initiative to improve his/her work
                                                                            performance and productivity?                                                                                                                                                                                                                                             
                                                                        </li>
                                                                        <li>
                                                                            Did the employee explore new ideas and methods to reach
                                                                            outcomes?
                                                                        </li>                                                                                        
                                                                        <li>
                                                                            Was the employee able to think out of the box?                                                                                                                                                                    
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        '>Value Add / Innovation</th>
                                                        <td class="text-center">10%</td>
                                                    </tr>
                                                    <tr>
                                                        <th class="w-75" data-toggle='popover' data-html="true" data-placement="bottom" data-trigger="hover"
                                                        title='<strong>Integrity / Ethics / Professionalism</strong>' 
                                                        data-content='
                                                            <div class="col-12">
                                                                <div class="row">
                                                                    Evaluate whether the employee
                                                                    adheres to high standards of
                                                                    personal and professional conduct.                                                                          
                                                                </div>
                                                                <hr/>
                                                                <div class="row">
                                                                    <ul>
                                                                        <li>
                                                                            Did the employee protect company intellectual property and
                                                                            company interest all the time?                                                                        
                                                                        </li>
                                                                        <li> 
                                                                            Did the employee maintain confidentiality of clientele’s
                                                                            information?                                                                                                                                                                                                                                            
                                                                        </li>
                                                                        <li>
                                                                            Was the employee trustworthy and honest?
                                                                        </li>                                                                                        
                                                                        <li>
                                                                            Did the employee exhibit professionalism and code of ethics in
                                                                            handling work / supervisors / coworkers / customers?                                                                                                                                                                                                                                                       
                                                                        </li>
                                                                        <li>
                                                                            Did the employee take shortcut in his/her job duties that caused
                                                                            product quality to be compromised? 
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        '>Integrity / Ethics / Professionalism</th>
                                                        <td class="text-center">10%</td>
                                                    </tr>
                                                    <tr>
                                                        <th class="w-50 text-right pr-5">Total Weightage from all Category</th>
                                                        <th class="text-center">30%</th>
                                                    <tr>
                                                </table>
                                            </div>
                                            <div class="tab-pane table-responsive p-0" id="attitude">
                                                <h3 class="ml-1 pb-2">Attitude</h3>
                                                <table class="table table-hover table-striped table-bordered">
                                                    <tr>
                                                        <th></th>
                                                        <th class="text-center">Weightage</th>
                                                    </tr>
                                                    <tr >
                                                        <th class="w-75" data-toggle='popover' data-html="true" data-placement="bottom" data-trigger="hover"
                                                        title='<strong>Teamwork</strong>' 
                                                        data-content='
                                                            <div class="col-12">
                                                                <div class="row">
                                                                    Evaluate the employee’s ability to
                                                                    work with others, interacts
                                                                    effectively and builds respectful
                                                                    relationships among coworkers                                                                           
                                                                </div>
                                                                <hr/>
                                                                <div class="row">
                                                                    <ul>
                                                                        <li>
                                                                            Does the employee exhibit cooperation in working with others?                                                                                 
                                                                        </li>
                                                                        <li> 
                                                                            Does the employee offer support to others when needed and help
                                                                            to promote a harmonious working environment?                                                                                                                                                               
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        '>Teamwork</th>
                                                        <td class="text-center">20%</td>
                                                    </tr>
                                                    <tr>
                                                        <th class="w-75" data-toggle='popover' data-html="true" data-placement="bottom" data-trigger="hover"
                                                        title='<strong>Diligence</strong>' 
                                                        data-content='
                                                            <div class="col-12">
                                                                <div class="row">
                                                                    Evaluate the employee’s constant
                                                                    and earnest effort to accomplish
                                                                    what is undertaken.                                                                      
                                                                </div>
                                                                <hr/>
                                                                <div class="row">
                                                                    <ul>
                                                                        <li>
                                                                            Is the employee hardworking?                                                                               
                                                                        </li>
                                                                        <li> 
                                                                            Does the employee always put in concerted effort to accomplish
                                                                            his/her assignments?                                                                                                                                                            
                                                                        </li>
                                                                        <li>
                                                                            Does the employee demonstrate perseverance in overcoming
                                                                            difficult challenges?
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        '>Dilligence</th>
                                                        <td class="text-center">20%</td>
                                                    </tr>
                                                    <tr>
                                                        <th class="w-75" data-toggle='popover' data-html="true" data-placement="bottom" data-trigger="hover"
                                                        title='<strong>Flexibility</strong>' 
                                                        data-content='
                                                            <div class="col-12">
                                                                <div class="row">
                                                                    Evaluate the employee’s ability to
                                                                    work with different personalities,
                                                                    different environments, respond
                                                                    positively to changes and adjust
                                                                    quickly to volatility.                                                                  
                                                                </div>
                                                                <hr/>
                                                                <div class="row">
                                                                    <ul>
                                                                        <li>
                                                                            Does the employee exhibit flexibility in working with others?                                                                               
                                                                        </li>
                                                                        <li> 
                                                                            Is the employee able to adapt to changes in work requirements,
                                                                            work environment, people, process, workload and deadlines?                                                                                                                                                           
                                                                        </li>
                                                                        <li>
                                                                            Does the employee show willingness to learn new knowledge and
                                                                            skills?                                                                                    
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        '>Flexibility</th>
                                                        <td class="text-center">20%</td>                                                
                                                    </tr>
                                                    <tr>
                                                        <th class="w-75" data-toggle='popover' data-html="true" data-placement="bottom" data-trigger="hover"
                                                        title='<strong>Responsible</strong>' 
                                                        data-content='
                                                            <div class="col-12">
                                                                <div class="row">
                                                                    Evaluate the employee’s
                                                                    dependability, trustworthiness, and
                                                                    consistency in keeping
                                                                    commitments.                                                                 
                                                                </div>
                                                                <hr/>
                                                                <div class="row">
                                                                    <ul>
                                                                        <li>
                                                                            Is the employee a responsible and reliable person?                                                                               
                                                                        </li>
                                                                        <li> 
                                                                            Does the employee take ownership of his/her work and
                                                                            responsibilities?                                                                                                                                                                                                                                             
                                                                        </li>
                                                                        <li>
                                                                            Does the employee admit his/her own mistakes and never give
                                                                            excuses or blame other people for own problem?                                                                                                                                                                       
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        '>Responsible</th>
                                                        <td class="text-center">20%</td>
                                                    </tr>
                                                    <tr>
                                                        <th class="w-75" data-toggle='popover' data-html="true" data-placement="bottom" data-trigger="hover"
                                                        title='<strong>Self-Initiative</strong>' 
                                                        data-content='
                                                            <div class="col-12">
                                                                <div class="row">
                                                                    Evaluate the employee’s motivation
                                                                    to act without having to be directed,
                                                                    being proactive instead of reactive
                                                                    in accepting additional
                                                                    responsibilities.                                                                
                                                                </div>
                                                                <hr/>
                                                                <div class="row">
                                                                    <ul>
                                                                        <li>
                                                                            Does the employee invite responsibility and perform function
                                                                            beyond the call of duty?                                                                               
                                                                        </li>
                                                                        <li> 
                                                                            Does the employee take ownership of problems including those
                                                                            not specifically assigned to him/her?                                                                                                                                                                                                                                   
                                                                        </li>
                                                                        <li>
                                                                            Is the employee often willing to go the extra mile to deliver his/her
                                                                            work and to help other colleagues?                                                                                                                                                                      
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        '>Self-Initiative</th>
                                                        <td class="text-center">10%</td>
                                                    </tr>
                                                    <tr>
                                                        <th class="w-75" data-toggle='popover' data-html="true" data-placement="bottom" data-trigger="hover"
                                                        title='<strong>Discipline</strong>' 
                                                        data-content='
                                                            <div class="col-12">
                                                                <div class="row">
                                                                    Evaluate the employee’s
                                                                    acceptance of management,
                                                                    policies, and department
                                                                    procedures.                                                             
                                                                </div>
                                                                <hr/>
                                                                <div class="row">
                                                                    <ul>
                                                                        <li>
                                                                            Does the employee accept the authority of management?                                                                               
                                                                        </li>
                                                                        <li> 
                                                                            Does the employee follow and abide by company policies and or
                                                                            department procedures?                                                                                                                                                                                                                                        
                                                                        </li>
                                                                        <li>
                                                                            Is the employee obedient in following supervisor’s instructions?                                                                                                                                                                     
                                                                        </li>
                                                                        <li>
                                                                            Does the employee observe the attendance and punctuality rules
                                                                            set by the company? 
                                                                        </li>
                                                                        <li>
                                                                            Does the employee enter effort data consistently and accurately
                                                                            into TLS?
                                                                        </li>
                                                                        <li>
                                                                            Does the employee enter defect data consistently and accurately
                                                                            into DSSi?
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        '>Discipline</th>
                                                        <td class="text-center">10%</td>
                                                    </tr>
                                                    <tr>
                                                        <th class="w-50 text-right pr-5">Total Weightage from all Category</th>
                                                        <th class="text-center">30%</th>
                                                    <tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row">                                        
                                            <div class="col-6">
                                                <h4 class="subheader">Ratings</h4>
                                                <table class="table table-hover table-bordered table-striped text-center">
                                                    <thead>
                                                        <th>Rating</th>
                                                        <th>Score</th>
                                                    <thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>Not Applicable</td>
                                                            <td>0</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Poor</td>
                                                            <td>1</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Below Average</td>
                                                            <td>1.5</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Average</td>
                                                            <td>2</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Good</td>
                                                            <td>2.5</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Excellent</td>
                                                            <td>3</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="col-6">
                                                <h4 class="subheader">Performance Band Mapping</h4>
                                                <div class="table-responsive">
                                                    <table class="table table-hover table-bordered table-striped text-center">
                                                        <thead>
                                                            <th>Performance Bands</th>
                                                            <th>Min Score</th>
                                                            <th>Max Score</th>
                                                        <thead>
                                                        <tbody>
                                                            <tr>
                                                                <td>A</td>
                                                                <td>2.8</td>
                                                                <td>3</td>
                                                            </tr>
                                                            <tr>
                                                                <td>B</td>
                                                                <td>2.5</td>
                                                                <td>&lt;2.8</td>
                                                            </tr>
                                                            <tr>
                                                                <td>C</td>
                                                                <td>2</td>
                                                                <td>&lt;2.5</td>
                                                            </tr>
                                                            <tr>
                                                                <td>D</td>
                                                                <td>1.5</td>
                                                                <td>&lt;2</td>
                                                            </tr>
                                                            <tr>
                                                                <td>E</td>
                                                                <td>1</td>
                                                                <td>&lt;1.5</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row"><!-- /.job guidelines row -->
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header d-flex p-0">
                                        <h3 class="card-title p-3">Job Grade Guideline</h3>
                                    </div>
                                    <!-- /.card-header -->
                                    <div class="card-body">
                                        <div class="tab-content">
                                            <div class="tab-pane active table-responsive p-0" id="capability">
                                                <table class="table table-hover table-striped table-bordered">
                                                    <thead>
                                                        <th>Category</th>
                                                        <th>Knowledge</th>
                                                        <th>Expectation of Responsibility</th>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <th >JSE / BSO</th>
                                                            <td class="w-50">Knows fundamental concepts / practices / procedures of a particular field of specialisation (e.g. programming or testing).</td>
                                                            <td>
                                                                <ul>
                                                                    <li>Performs assigned tasks using established procedures and working under immediate supervision.</li>
                                                                    <li>Work is routine and instructions are usually detailed.</li>
                                                                </ul>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <th >SSE / SBSO</th>
                                                            <td class="w-50">Knows and applies the fundamental concepts / practices / procedures of a particular field of specialisation. Knows and applies the fundamental concepts / practices / procedures of a particular field of specialisation. Became more skillful and mature than the previous level.</td>
                                                            <td>
                                                                <ul>
                                                                    <li>Performs work under close supervision but usually involves limited responsibility.</li>
                                                                    <li>Work is routine and instructions are usually detailed.</li>
                                                                    <li>Some evaluation, ingenuity and resourcefulness is required.</li>
                                                                </ul>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <th >SC / BC</th>
                                                            <td class="w-50">Possesses and applies a broad knowledge of principles / practices / procedures of a particular field of specialisation.Possesses and applies a broad knowledge of principles / practices / procedures of a particular field of specialisation, as well as  fundamental concepts / practices / procedures of another specialisation (e.g. programming + design; testing + training)</td>       
                                                            <td>
                                                                <ul>
                                                                    <li>Usually works with minimum supervision, able to multitask & prioritise, and usually involves some responsibility.</li>
                                                                    <li>Assignments are broad, requiring ingenuity and resourcefulness.</li>
                                                                    <li>Has the opportunity to supervise/lead some juniors.</li>
                                                                </ul>
                                                            </td>                                         
                                                        </tr>
                                                        <tr>
                                                            <th >SSC / SBC</th>
                                                            <td class="w-50">'Possesses and applies comprehensive & in-depth knowledge of principles / practices / procedures of multiple specialisations to the completion of complex assignments. Able to act as a domain specialist / subject matter expert in one or more specialisation to spearhead value-added initiatives.</td>
                                                            <td>
                                                                <ul>
                                                                    <li>Performs more challenging work in multiple aspects and carries more responsibilities under general supervision.</li>
                                                                    <li>Plans, executes and supervises assignments. </li>
                                                                    <li>Reviews progress and evaluates results.</li>
                                                                    <li>Plans and assigns personnel for given projects or tasks.</li>
                                                                    <li>Recommends changes/improvements in procedures.</li>
                                                                    <li>Has the opportunity to lead a team.</li>
                                                                </ul>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <th >MGR / AVP</th>
                                                            <td class="w-50">Possesses and applies comprehensive knowledge of principles / practices / procedures of multiple fields / specialisations to the completion of significant assignments / projects.</td>
                                                            <td>
                                                                <ul>
                                                                    <li>Plans and executes assignments, team leading or manage projects.</li>
                                                                    <li>Reviews progress and evaluates results.</li>
                                                                    <li>Assists in review and evaluation of personnel performance</li>
                                                                    <li>Assists in departmental staff development, management and motivation.</li>
                                                                    <li>Recommends major changes/improvements in procedures.</li>
                                                                </ul>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>    
                                        </div>
                                    </div>
                                    <!-- /.card-body -->
                                </div>
                                <!-- /.card -->
                            </div>
                        </div><!-- /.job guidlines row END -->
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection