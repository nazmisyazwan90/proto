<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

// main project routes goes here

Route::get('/admin/admin-dash', 'DashboardController@admin_dash')->name('admin-dash');
Route::get('/staff/staff-dash', 'StaffDashboardController@staff_dash')->name('staff-dash');
Route::get('/appr/appr-dash', 'DashboardController@appr_dash')->name('appr-dash');
Route::get('/review/review-dash', 'DashboardController@review_dash')->name('review-dash');

Route::get('/staff/past-appraisal', 'StaffPastAppraisalController@index')->name('past-appraisal');
Route::get('/staff/self-appraisal', 'SelfAppraisalFormController@index')->name('self-appraisal');

Route::get('/admin/guidelines', 'GuidelinesController@guideline')->name('guideline');
Route::get('/admin/edit-guidelines', 'GuidelinesController@edit')->name('edit-guideline');

Route::get('/admin/admin-details', 'AdminChartController@index')->name('admin-details');
Route::get('/admin/guidelines/2', 'GuidelinesController@done')->name('edit-done');

Route::get('/appr/appraise-list', 'StaffListController@appraise_list')->name('appraise-list');
Route::get('/appr/staff-appraisal', 'StaffAppraiseFormController@index')->name('appraise-form');


Route::get('/review/review-list', 'StaffListController@review_list')->name('review-list');
Route::get('/review/staff-appraisal-review', 'StaffAppraiseFormController@review')->name('review-form');

Route::get('/staff-profile', function(){
    return view('content.staff.profile');
});

Route::get('/staff/staff-dash/done', 'SelfAppraisalFormController@test')->name('self-submit');
Route::get('/appr/staff-appraisal/done', 'StaffAppraiseFormController@test')->name('submit-form');

// dashboards
Route::get('/dashboard/home', 'DashboardController@versionone')->name('v1');
Route::get('/dashboard/v2', 'DashboardController@versiontwo')->name('v2');
Route::get('/dashboard/v3', 'DashboardController@versionthree')->name('v3');

// content
Route::get('/widgets', 'ContentController@showwidgetpage')->name('widget');
Route::get('/calendar', 'ContentController@showcalendarpage')->name('calendar');

// charts
Route::get('/charts/chartjs', 'ContentController@showchartjspage')->name('chartjs');
Route::get('/charts/flot', 'ContentController@showflotpage')->name('flot');
Route::get('/charts/inline', 'ContentController@showinlinepage')->name('inline');

// ui elements
Route::get('/ui/general', 'ContentController@showgeneralpage')->name('general');
Route::get('/ui/icons', 'ContentController@showiconpage')->name('icons');
Route::get('/ui/buttons', 'ContentController@showbuttonspage')->name('buttons');
Route::get('/ui/sliders', 'ContentController@showsliderspage')->name('sliders');

// forms
Route::get('/forms/generalelements', 'ContentController@showgeneralelementspage')->name('generalelements');
Route::get('/forms/advanced', 'ContentController@showadvancedpage')->name('advanced');
Route::get('/forms/editors', 'ContentController@showeditorspage')->name('editors');

//tables
Route::get('/tables/simpletables', 'ContentController@showsimpletablespage')->name('simpletables');
Route::get('/tables/datatables', 'ContentController@showdatatablespage')->name('datatables');

//mailbox
Route::get('/mailbox/inbox', 'ContentController@showinboxpage')->name('inbox');
Route::get('/mailbox/compose', 'ContentController@showcomposepage')->name('compose');
Route::get('/mailbox/readmail', 'ContentController@showreadmailpage')->name('readmail');

//pages
Route::get('/pages/invoice', 'ContentController@showinvoicepage')->name('invoice');
Route::get('/pages/profile', 'ContentController@showprofilepage')->name('profile');
Route::get('/pages/lockscreen', 'ContentController@showlockscreenpage')->name('lockscreen');
Route::get('/pages/invoice_print', 'ContentController@showinvoiceprint')->name('invoice_print');

//extras
Route::get('/extras/starter', 'ContentController@showstarterpage')->name('starter');
Route::get('/extras/404', 'ContentController@show404page')->name('404');
Route::get('/extras/500', 'ContentController@show500page')->name('500');
Route::get('/extras/blank', 'ContentController@showblankpage')->name('blank');

//testpage
Route::get('/test/test1', 'ContentController@showtestpage')->name('test1');

Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');
