<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsOnUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table){
            $table->date('joined_date');
            $table->string('staff_type');
            $table->string('pos_id');
            $table->string('dept_id');
            $table->date('last_promotion_date');
            $table->string('staff_role')->nullable();


            $table->foreign('pos_id')->references('pos_id')->on('positions');
            $table->foreign('dept_id')->references('dept_id')->on('departments');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table){
            $table->dropForeign('users_positions_pos_id_foreign');
            $table->dropForeign('users_departments_dept_id_foreign');
            
            $table->dropColumn('joined_date');
            $table->dropColumn('staff_type');
            $table->dropColumn('pos_id');
            $table->dropColumn('dept_id');
            $table->dropColumn('last_promotion_date');
            $table->dropColumn('staff_role');

            
        });
    }
}
