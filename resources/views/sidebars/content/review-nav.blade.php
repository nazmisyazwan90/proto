<li class="nav-item has-treeview {!! classActivePath(1, 'review') !!}">
    <a href="#" class="nav-link {!! classActiveSegment(1, 'review') !!}">
      <i class="nav-icon fa fa-book"></i>
      <p>
        Reviewer
        <i class="fa fa-angle-left right"></i>
      </p>
    </a>
    <ul class="nav nav-treeview">
        <li class="nav-item">
            <a href={{ url('/review/review-dash') }} class="nav-link {!! classActiveSegment(2, 'review-dash') !!} treeview-first">
                <i class="fa fa-circle-o nav-icon"></i>
                <p>Dashboard</p>
            </a>
        </li>
        <li class="nav-item">
            <a href={{ url('/review/review-list') }} class="nav-link {!! classActiveSegment(2, 'review-list') !!} treeview-first">
                <i class="fa fa-circle-o nav-icon"></i>
                <p>Appraisal Review List</p>
            </a>
        </li>
        <li class="nav-item">
            <a href={{ url('/review/staff-appraisal-review') }} class="nav-link {!! classActiveSegment(2, 'staff-appraisal-review') !!} treeview-first">
                <i class="fa fa-circle-o nav-icon"></i>
                <p>Review Form</p>
            </a>
        </li>
    </ul>
</li>