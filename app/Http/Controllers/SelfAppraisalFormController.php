<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SelfAppraisalFormController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {   
        return view('content.staff.self-appraisal');
    }
    public function test(Request $request)
    {
        switch($request->button){
            case 'save':
                return redirect()->route('self-appraisal')->with('success', 'Self Appraisal Saved');
            break;            
            case 'submit':
                return redirect()->route('staff-dash')->with('success', 'Self Appraisal Submitted');
            break;
        }
    }
}
