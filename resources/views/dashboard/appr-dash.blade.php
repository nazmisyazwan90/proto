@extends('layouts.mastercontent')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
            <h1>Admin Dashboard</h1>
            </div>
            <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item active">Blank Page</li>
            </ol>
            </div>
        </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <section class="col-12">
                <h1 class="mt-2 ml-2 mb-4">Hello, Nazmi!</h1>
            </section>
        </div>
        <div class="row">
            <section class="col-7">
                <div class="row">
                    <section class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">My Details</h3>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <section class="col-3 text-center">
                                        - Picture Here -
                                    </section>
                                    <section class="col-9">
                                        <p class="mb-3"><span class="mr-2" style="font-weight: bold">Name:</span>Nazmi Syazwan Bin Mohamad Zin<br></p>
                                        <p class="mb-3"><span class="mr-2" style="font-weight: bold">E-mail:</span>nazmisyazwan@hotmail.com</p>
                                        <p class="mb-3"><span class="mr-2" style="font-weight: bold">Department:</span>Internal</p>
                                        <p class="mb-3"><span class="mr-2" style="font-weight: bold">Position:</span>System Engineer</p>
                                        <p class="mb-3"><span class="mr-2" style="font-weight: bold">Joined Date:</span>22 May 2015</p>
                                        <p class="mb-3"><span class="mr-2" style="font-weight: bold">Last Promoted:</span>15 June 2016</p>
                                    </section>
                                </div>
                            </div>
                        </div>           
                    </section>
                </div>        
            </section>
            <section class="col-5 align-content-right">
                <div class="card">
                    <div class="card-body text-center align-content-center">
                        <p>Self Appraisal Status:</p>
                        <p style="color:red; font-weight:bold">Incomplete</p>
                    </div>
                </div>
                <a class="btn btn-block btn-success btn-lg" href="{{ route('self-appraisal') }}">Self Appraisal Form</a>
                <a class="btn btn-block btn-primary btn-lg" href="{{ route('past-appraisal') }}">Past Appraisal(s)</a>
                <a class="btn btn-block btn-primary btn-lg" style="background-color: darkorange; color: white;" href="{{ route('appraise-list') }}">Appraise Staff</a>
            </section>
        </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection