@guest
    <ul class="navbar-nav ml-auto">
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="" id="dropdownMenuButton" data-toggle="dropdown">
                Login  
            </a>
            <div class="dropdown-menu dropdown-menu-right" style="min-width: 400%">
                <form class="px-4 py-3" action="{{ route('login') }}" method="post">
                    @csrf
                    <div class="form-group">
                        <label for="username">Username</label>
                        <div class="input-group-append">
                            <input id="username" type="username" class="form-control{{ $errors->has('username') ? ' is-invalid' : '' }}" placeholder="Username" name="username" value="{{ old('username') }}" required autofocus>
                            <span class="fa fa-envelope input-group-text"></span> 
                            @if ($errors->has('username'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('username') }}</strong>
                                </span> 
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="password">Password</label>
                        <div class="input-group-append">
                            <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="password" name="password" required>
                            <span class="fa fa-lock input-group-text"></span> 
                            @if ($errors->has('password'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span> 
                            @endif
                        </div>                        
                    </div>
                    <div class="form-check">
                        <input type="checkbox" class="form-check-input" id="dropdownCheck">
                        <label class="form-check-label mb-2" for="dropdownCheck">
                            Remember Me
                        </label>
                    </div>
                    <button type="submit" class="btn btn-primary">Sign in</button>
                </form>
            </div>
        </li>
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="" id="dropdownMenuButton" data-toggle="dropdown">
                Register 
            </a>
            <div class="dropdown-menu dropdown-menu-right" style="min-width: 350%">
                    <form class="px-4 py-3" action="{{ route('register') }}" method="post">
                        @csrf
                        <label for="username">Username</label>
                        <div class="input-group mb-3">
                            <input id="username" type="username" class="form-control{{ $errors->has('username') ? ' is-invalid' : '' }}" name="username" value="{{ old('username') }}" placeholder="Your Username">
                            <div class="input-group-append">
                            <span class="fa fa-envelope input-group-text"></span> 
                            @if ($errors->has('username'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('username') }}</strong>
                                </span> 
                            @endif
                            </div>
                        </div>
                        <label for="name">Full Name</label>
                        <div class="input-group mb-3">
                            <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" placeholder="Enter Your Full Name"
                            required autofocus> @if ($errors->has('name'))
                            <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('name') }}</strong>
                                                    </span> @endif
                            <div class="input-group-append">
                            <span class="fa fa-user input-group-text"></span>
                            </div>
                        </div>
                        <label for="email">Password</label>
                        <div class="input-group mb-3">
                            <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Password" required>
                            <div class="input-group-append">
                            <span class="fa fa-lock input-group-text"></span>
                            </div>
                        </div>
                        <div class="input-group mb-3">
                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Confirm Password" required>
                            <div class="input-group-append">
                            <span class="fa fa-lock input-group-text"></span> 
                            @if ($errors->has('password'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span> 
                            @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                            <button type="submit" class="btn btn-primary">Register</button>
                            </div>
                            <!-- /.col -->
                        </div>
                    </form>
            </div> 
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ url('/login') }}">Login</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ url('/register') }}">Register</a>
        </li>
    </ul>
@else
    <ul class="navbar-nav ml-auto">
        <li class="nav-item">
            <a class="nav-link" href="{{ url('/logout') }}">Logout</a>
        </li>
    </ul>
@endguest
